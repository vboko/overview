package xyz.gateguard.tablettenant.util;

import android.app.Activity;
import android.app.admin.DeviceAdminReceiver;
import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.annotation.RequiresApi;

/**
 * Created by D on 18/09/2017.
 */


    public class UpdaterDeviceAdminReceiver extends DeviceAdminReceiver {

    private static final String APP_PREF = "APP_PREF";
    private DevicePolicyManager mDPM;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);

        SharedPreferences prefs = context.getSharedPreferences(APP_PREF, Activity.MODE_PRIVATE);
        prefs.edit().clear().commit();

//        mDPM.setLockTaskPackages();
//        mDPM.setDeviceOwnerLockScreenInfo();
//        mDPM.setPackagesSuspended()
    }

    @Override
    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
    }
}
