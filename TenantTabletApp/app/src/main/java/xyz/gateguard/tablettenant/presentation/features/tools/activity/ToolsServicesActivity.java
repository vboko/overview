package xyz.gateguard.tablettenant.presentation.features.tools.activity;

import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.tools.fragment.ToolsServicesFragment;
import xyz.gateguard.tablettenant.util.MyPrefs;


public class ToolsServicesActivity extends BaseActivity {
    private ProgressDialog progressDialog;

    public static void startActivity(Context activity) {
        Intent intent = new Intent(activity, ToolsServicesActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);

         setCurrentLocale();
        startToolsServicesFragment();

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);
        notificationManager.cancelAll();


        if (MyPrefs.getPushToken(this).length() > 0)
            ApiController.setToken();
    }

    private void startToolsServicesFragment() {
        ToolsServicesFragment toolsServicesFragment = (ToolsServicesFragment) getSupportFragmentManager().findFragmentByTag(ToolsServicesFragment.TAG);
        if (toolsServicesFragment == null) {
            toolsServicesFragment = ToolsServicesFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.empty_frame_layout, toolsServicesFragment, ToolsServicesFragment.TAG)
                    .commit();
        }
    }
}