package xyz.gateguard.tablettenant.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dmitriy Boichuk on 13.05.2017.
 */

public class GuestList {
    @SerializedName("error")
    @Expose
    private Error error;

    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }



    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    public class Datum {

        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("restrictions")
        @Expose
        private Restrictions restrictions;
        @SerializedName("guest_data")
        @Expose
        private GuestData guestData;
        @SerializedName("guest_unique_id")
        @Expose
        private String guestUniqueId;

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Restrictions getRestrictions() {
            return restrictions;
        }

        public void setRestrictions(Restrictions restrictions) {
            this.restrictions = restrictions;
        }

        public GuestData getGuestData() {
            return guestData;
        }

        public void setGuestData(GuestData guestData) {
            this.guestData = guestData;
        }

        public String getGuestUniqueId() {
            return guestUniqueId;
        }

        public void setGuestUniqueId(String guestUniqueId) {
            this.guestUniqueId = guestUniqueId;
        }

    }


    public class GuestData {

        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile_phone")
        @Expose
        private String mobilePhone;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobilePhone() {
            return mobilePhone;
        }

        public void setMobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
        }

    }

    public class Restrictions {

        @SerializedName("sd")
        @Expose
        private String sd;
        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("date_start")
        @Expose
        private String dateStart;
        @SerializedName("date_end")
        @Expose
        private String dateEnd;
        @SerializedName("time_start")
        @Expose
        private String timeStart;
        @SerializedName("time_end")
        @Expose
        private String timeEnd;
        @SerializedName("days")
        @Expose
        private List<Integer> days = null;

        public String getSd() {
            return sd;
        }

        public void setSd(String sd) {
            this.sd = sd;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDateStart() {
            return dateStart;
        }

        public void setDateStart(String dateStart) {
            this.dateStart = dateStart;
        }

        public String getDateEnd() {
            return dateEnd;
        }

        public void setDateEnd(String dateEnd) {
            this.dateEnd = dateEnd;
        }

        public String getTimeStart() {
            return timeStart;
        }

        public void setTimeStart(String timeStart) {
            this.timeStart = timeStart;
        }

        public String getTimeEnd() {
            return timeEnd;
        }

        public void setTimeEnd(String timeEnd) {
            this.timeEnd = timeEnd;
        }

        public List<Integer> getDays() {
            return days;
        }

        public void setDays(List<Integer> days) {
            this.days = days;
        }

    }



}
