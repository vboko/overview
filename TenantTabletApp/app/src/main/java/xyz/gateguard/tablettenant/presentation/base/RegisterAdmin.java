package xyz.gateguard.tablettenant.presentation.base;

import android.app.Activity;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.List;

import xyz.gateguard.tablettenant.AppsManager;
import xyz.gateguard.tablettenant.Constants;
import xyz.gateguard.tablettenant.PolicyAdmin;
import xyz.gateguard.tablettenant.R;

import static android.app.admin.DevicePolicyManager.EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME;

/**
 * Created by D on 21/09/2017.
 */

public class RegisterAdmin {

    private static final String TAG = "RegisterAdmin";
    private Activity activity;
    private ProgressBar mProgressBar;
    private AppsManager mAppsManager = AppsManager.getInstance();
    //    private PackageManager mPackageManager = getApplicationContext().getPackageManager();
    private Context mContext;
    private static final int REQ_ACTIVATE_DEVICE_ADMIN = 10;
    DevicePolicyManager mDpm;
    private static final int REQUEST_PROVISION_MANAGED_PROFILE = 1;

    public RegisterAdmin(Context context, Activity activity) {
        this.mContext = context;
        this.activity = activity;
    }


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void activateAdmin() {
        ComponentName deviceAdmin = new ComponentName(mContext, PolicyAdmin.class);

        mDpm = (DevicePolicyManager) mContext.getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!mDpm.isAdminActive(deviceAdmin)) {
            Toast.makeText(mContext, activity.getApplication().getString(R.string.not_device_admin), Toast.LENGTH_LONG).show();
        }
        if (!mAppsManager.isAdminActive()) {
            Intent activateDeviceAdminIntent =
                    new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
            activateDeviceAdminIntent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN,
                    mAppsManager.getAdminComponent());
            activateDeviceAdminIntent.putExtra(EXTRA_PROVISIONING_DEVICE_ADMIN_PACKAGE_NAME, mContext.getApplicationContext().getPackageName());
            if (activateDeviceAdminIntent.resolveActivity(activity.getPackageManager()) != null) {
                activity.startActivityForResult(activateDeviceAdminIntent, REQUEST_PROVISION_MANAGED_PROFILE);
                activity.finish();
            } else {
                Toast.makeText(activity, "Stopping.", Toast.LENGTH_SHORT).show();
            }
            // It is good practice to include the optional explanation text to explain to
            // user why the application is requesting to be a device administrator.  The system
            // will display this message on the activation screen.
//            activateDeviceAdminIntent.putExtra(DevicePolicyManager.EXTRA_ADD_EXPLANATION,
//                    activity.getResources().getString(R.string.device_admin_activation_message));
//            activity.startActivityForResult(activateDeviceAdminIntent, REQ_ACTIVATE_DEVICE_ADMIN);
        }

        if (mDpm.isDeviceOwnerApp(activity.getPackageName())) {
            mDpm.setLockTaskPackages(deviceAdmin, new String[]{activity.getPackageName()});
        } else {
            Toast.makeText(mContext, activity.getApplication().getString(R.string.not_device_owner), Toast.LENGTH_LONG).show();
        }

        hideApps();
        try {
            setMobileConnectionEnabled(mContext, false);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
    }

    private void setMobileConnectionEnabled(Context context, boolean enabled) throws ClassNotFoundException, NoSuchFieldException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {
        final ConnectivityManager mConnectivityManager = (ConnectivityManager)  context.getSystemService(Context.CONNECTIVITY_SERVICE);
        final Class mClass = Class.forName(mConnectivityManager.getClass().getName());
        final Field mField = mClass.getDeclaredField("mService");
        mField.setAccessible(true);
        final Object mObject = mField.get(mConnectivityManager);
        final Class mConnectivityManagerClass =  Class.forName(mObject.getClass().getName());
        final Method setMobileDataEnabledMethod = mConnectivityManagerClass.getDeclaredMethod(Constants.MOBILE_BLOCKING_NAME, Boolean.TYPE);
        setMobileDataEnabledMethod.setAccessible(true);

        setMobileDataEnabledMethod.invoke(mObject, enabled);
    }

    public void hideApps() {
        Log.d(TAG, "isAdminActive " + mAppsManager.isAdminActive());
        if (mAppsManager.isAdminActive() && mAppsManager.isOwner()) {

            List<ApplicationInfo> appList = mAppsManager.getAppsList();
            for (ApplicationInfo app : appList) {
                //hide all apps except this app and launcher
//
                if (!app.packageName.equals(mContext.getApplicationContext().getPackageName())
                        && !app.packageName.equals(Constants.PACKAGE_LAUNCHER) && !app.packageName.equals(Constants.PACKAGE_SETTINGS)
                        && !app.packageName.equals("com.android.systemui")
                        && !app.packageName.equals("com.android.packageinstaller")
                        && !app.packageName.equals(("android"))
                        && !app.packageName.equals(("com.example.android.apis"))
                        && !app.packageName.equals(("com.example.android.apiscom.android.wallpaper"))
                        && !app.packageName.equals("com.android.galaxy4")
                        && !app.packageName.equals("com.android.inputdevices")
                        && !app.packageName.equals("com.android.development")
                        && !app.packageName.equals("com.android.provision")
                        && !app.packageName.equals("com.android.certinstaller")
                        && !app.packageName.equals("com.example.android.livecubes")
                        && !app.packageName.equals("com.android.defcontainer")
                        && !app.packageName.equals("com.android.backupconfirm")
                        && !app.packageName.equals("com.amaze.filemanager")
                        && !app.packageName.equals("com.android.customlocale2")
                        && !app.packageName.equals("com.android.captiveportallogin")
                        && !app.packageName.equals("com.android.inputdevices")
                        && !app.packageName.equals("com.android.vpndialogs")
                        && !app.packageName.equals("com.android.inputmethod.latin")
                        && !app.packageName.equals("com.android.inputdevices")
                        && !app.packageName.equals("com.android.com.updaterapp")
                        && !app.packageName.equals("com.TNUpdaterapp")
                        ) {
                    mAppsManager.hideApp(app.packageName);
                }
            }
//
        }
    }
}
