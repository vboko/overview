package xyz.gateguard.tablettenant.presentation.features.account.fragmentview;

public interface IAccountFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void back();

        void logout();
        void onAddUnitClick();

        void onEditUnitClick();

        void onChangePasClick();

        void onChangePinClick();

        void onChangeEmailClick();

        void onChangeLangClick();

        void onUpdatePaymentInfoClick();

        void onUpdateShippingAccountClick();

        void onSelectLanguageClick();
    }
}
