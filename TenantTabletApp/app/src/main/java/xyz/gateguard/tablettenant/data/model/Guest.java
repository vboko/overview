package xyz.gateguard.tablettenant.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Dmitriy Boichuk on 16.05.2017.
 */

public class Guest {
    @SerializedName("error")
    @Expose
    private Error error;

    public Error getError() {
        return error;
    }

    public void setError(Error error) {
        this.error = error;
    }

    @SerializedName("value")
    @Expose
    private String value;
    @SerializedName("restrictions")
    @Expose
    private Restrictions restrictions;
    @SerializedName("guest_data")
    @Expose
    private GuestData guestData;
    @SerializedName("guest_unique_id")
    @Expose
    private String guestUniqueId;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Restrictions getRestrictions() {
        return restrictions;
    }

    public void setRestrictions(Restrictions restrictions) {
        this.restrictions = restrictions;
    }

    public GuestData getGuestData() {
        return guestData;
    }

    public void setGuestData(GuestData guestData) {
        this.guestData = guestData;
    }

    public String getGuestUniqueId() {
        return guestUniqueId;
    }

    public void setGuestUniqueId(String guestUniqueId) {
        this.guestUniqueId = guestUniqueId;
    }


    public class GuestData {

        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("mobile_phone")
        @Expose
        private String mobilePhone;

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobilePhone() {
            return mobilePhone;
        }

        public void setMobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
        }

    }

    public class Restrictions {

        @SerializedName("type")
        @Expose
        private String type;
        @SerializedName("date_start")
        @Expose
        private String dateStart;
        @SerializedName("date_end")
        @Expose
        private String dateEnd;
        @SerializedName("time_start")
        @Expose
        private String timeStart;
        @SerializedName("time_end")
        @Expose
        private String timeEnd;
        @SerializedName("days")
        @Expose
        private List<Integer> days = null;

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getDateStart() {
            return dateStart;
        }

        public void setDateStart(String dateStart) {
            this.dateStart = dateStart;
        }

        public String getDateEnd() {
            return dateEnd;
        }

        public void setDateEnd(String dateEnd) {
            this.dateEnd = dateEnd;
        }

        public String getTimeStart() {
            return timeStart;
        }

        public void setTimeStart(String timeStart) {
            this.timeStart = timeStart;
        }

        public String getTimeEnd() {
            return timeEnd;
        }

        public void setTimeEnd(String timeEnd) {
            this.timeEnd = timeEnd;
        }

        public List<Integer> getDays() {
            return days;
        }

        public void setDays(List<Integer> days) {
            this.days = days;
        }

    }
}
/*
    @SerializedName("data")
    @Expose
    private Datum data = null;

    public Datum getData() {
        return data;
    }

    public void setData(Datum data) {
        this.data = data;
    }

    public class Pin {

        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("device_id")
        @Expose
        private Integer deviceId;
        @SerializedName("value")
        @Expose
        private String value;
        @SerializedName("active")
        @Expose
        private Boolean active;
        @SerializedName("restrictions")
        @Expose
        private Restrictions restrictions;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(Integer deviceId) {
            this.deviceId = deviceId;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public Boolean getActive() {
            return active;
        }

        public void setActive(Boolean active) {
            this.active = active;
        }

        public Restrictions getRestrictions() {
            return restrictions;
        }

        public void setRestrictions(Restrictions restrictions) {
            this.restrictions = restrictions;
        }

    }
    public class Pivot {

        @SerializedName("user_id")
        @Expose
        private Integer userId;
        @SerializedName("device_id")
        @Expose
        private Integer deviceId;

        public Integer getUserId() {
            return userId;
        }

        public void setUserId(Integer userId) {
            this.userId = userId;
        }

        public Integer getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(Integer deviceId) {
            this.deviceId = deviceId;
        }

    }
    public class Restrictions {


        @SerializedName("date_start")
        @Expose
        private String dateStart;
        @SerializedName("date_end")
        @Expose
        private String dateEnd;
        @SerializedName("time_start")
        @Expose
        private String timeStart;
        @SerializedName("time_end")
        @Expose
        private String timeEnd;
        @SerializedName("days")
        @Expose
        private List<Integer> days = null;
        @SerializedName("type")
        @Expose
        private String type;

        public String getDateStart() {
            return dateStart;
        }

        public void setDateStart(String dateStart) {
            this.dateStart = dateStart;
        }

        public String getDateEnd() {
            return dateEnd;
        }

        public void setDateEnd(String dateEnd) {
            this.dateEnd = dateEnd;
        }

        public String getTimeStart() {
            return timeStart;
        }

        public void setTimeStart(String timeStart) {
            this.timeStart = timeStart;
        }

        public String getTimeEnd() {
            return timeEnd;
        }

        public void setTimeEnd(String timeEnd) {
            this.timeEnd = timeEnd;
        }

        public List<Integer> getDays() {
            return days;
        }

        public void setDays(List<Integer> days) {
            this.days = days;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

    }
    public class UpdatedAt {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getTimezoneType() {
            return timezoneType;
        }

        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }

    public class Datum {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("password")
        @Expose
        private String password;
        @SerializedName("remember_token")
        @Expose
        private Object rememberToken;
        @SerializedName("created_at")
        @Expose
        private CreatedAt createdAt;
        @SerializedName("updated_at")
        @Expose
        private UpdatedAt updatedAt;
        @SerializedName("title")
        @Expose
        private String title;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("middle_name")
        @Expose
        private String middleName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("suffix")
        @Expose
        private String suffix;
        @SerializedName("date_of_birth")
        @Expose
        private String dateOfBirth;
        @SerializedName("pp_user_id")
        @Expose
        private Object ppUserId;
        @SerializedName("mobile_phone")
        @Expose
        private String mobilePhone;
        @SerializedName("devices")
        @Expose
        private List<Device> devices = null;
        @SerializedName("pins")
        @Expose
        private List<Pin> pins = null;
        @SerializedName("contacts")
        @Expose
        private List<Object> contacts = null;
        @SerializedName("co_tenants")
        @Expose
        private List<Object> coTenants = null;
        @SerializedName("documents")
        @Expose
        private List<Object> documents = null;
        @SerializedName("notes")
        @Expose
        private List<Object> notes = null;
        @SerializedName("lease")
        @Expose
        private Object lease;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public Object getRememberToken() {
            return rememberToken;
        }

        public void setRememberToken(Object rememberToken) {
            this.rememberToken = rememberToken;
        }

        public CreatedAt getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(CreatedAt createdAt) {
            this.createdAt = createdAt;
        }

        public UpdatedAt getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(UpdatedAt updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getSuffix() {
            return suffix;
        }

        public void setSuffix(String suffix) {
            this.suffix = suffix;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public Object getPpUserId() {
            return ppUserId;
        }

        public void setPpUserId(Object ppUserId) {
            this.ppUserId = ppUserId;
        }

        public String getMobilePhone() {
            return mobilePhone;
        }

        public void setMobilePhone(String mobilePhone) {
            this.mobilePhone = mobilePhone;
        }

        public List<Device> getDevices() {
            return devices;
        }

        public void setDevices(List<Device> devices) {
            this.devices = devices;
        }

        public List<Pin> getPins() {
            return pins;
        }

        public void setPins(List<Pin> pins) {
            this.pins = pins;
        }

        public List<Object> getContacts() {
            return contacts;
        }

        public void setContacts(List<Object> contacts) {
            this.contacts = contacts;
        }

        public List<Object> getCoTenants() {
            return coTenants;
        }

        public void setCoTenants(List<Object> coTenants) {
            this.coTenants = coTenants;
        }

        public List<Object> getDocuments() {
            return documents;
        }

        public void setDocuments(List<Object> documents) {
            this.documents = documents;
        }

        public List<Object> getNotes() {
            return notes;
        }

        public void setNotes(List<Object> notes) {
            this.notes = notes;
        }

        public Object getLease() {
            return lease;
        }

        public void setLease(Object lease) {
            this.lease = lease;
        }

    }

    public class Device {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("hash")
        @Expose
        private String hash;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_at")
        @Expose
        private String createdAt;
        @SerializedName("updated_at")
        @Expose
        private String updatedAt;
        @SerializedName("bbl")
        @Expose
        private String bbl;
        @SerializedName("root_device_id")
        @Expose
        private Object rootDeviceId;
        @SerializedName("pivot")
        @Expose
        private Pivot pivot;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getHash() {
            return hash;
        }

        public void setHash(String hash) {
            this.hash = hash;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getBbl() {
            return bbl;
        }

        public void setBbl(String bbl) {
            this.bbl = bbl;
        }

        public Object getRootDeviceId() {
            return rootDeviceId;
        }

        public void setRootDeviceId(Object rootDeviceId) {
            this.rootDeviceId = rootDeviceId;
        }

        public Pivot getPivot() {
            return pivot;
        }

        public void setPivot(Pivot pivot) {
            this.pivot = pivot;
        }

    }


    public class CreatedAt {

        @SerializedName("date")
        @Expose
        private String date;
        @SerializedName("timezone_type")
        @Expose
        private Integer timezoneType;
        @SerializedName("timezone")
        @Expose
        private String timezone;

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public Integer getTimezoneType() {
            return timezoneType;
        }

        public void setTimezoneType(Integer timezoneType) {
            this.timezoneType = timezoneType;
        }

        public String getTimezone() {
            return timezone;
        }

        public void setTimezone(String timezone) {
            this.timezone = timezone;
        }

    }*/


