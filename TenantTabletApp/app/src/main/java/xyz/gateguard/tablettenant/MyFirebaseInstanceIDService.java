package xyz.gateguard.tablettenant;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.util.MyPrefs;


/**
 * Created by Boichuk Dmitriy on 29.06.2016.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        sendRegistrationToServer(refreshedToken);
    }

    private void sendRegistrationToServer(String token) {
        MyPrefs.setPushToken(getApplicationContext(), token);
        ApiController.setToken();
    }
}