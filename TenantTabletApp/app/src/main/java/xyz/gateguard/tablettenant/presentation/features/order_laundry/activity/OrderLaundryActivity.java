package xyz.gateguard.tablettenant.presentation.features.order_laundry.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.order_laundry.fragment.OrderLaundryFragment;

public class OrderLaundryActivity extends BaseActivity {

    public static void startActivity(FragmentActivity activity) {
        Intent intent = new Intent(activity, OrderLaundryActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));

        startOrderLaundryFragment();
    }

    private void startOrderLaundryFragment() {
        OrderLaundryFragment orderLaundryFragment = (OrderLaundryFragment) getSupportFragmentManager().findFragmentByTag(OrderLaundryFragment.TAG);
        if (orderLaundryFragment == null) {
            orderLaundryFragment = OrderLaundryFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.empty_frame_layout, orderLaundryFragment, OrderLaundryFragment.TAG)
                    .commit();
        }
    }

}
