package xyz.gateguard.tablettenant.presentation.features.buzz.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseFragment;
import xyz.gateguard.tablettenant.presentation.features.buzz.fragmentview.IBuzzFragmentView;
import xyz.gateguard.tablettenant.presentation.features.login.fragment.SignInFragment;

public class BuzzFragment extends BaseFragment {

    public static final String TAG = BuzzFragment.class.getSimpleName();

    public static BuzzFragment newInstance() {
        return new BuzzFragment();
    }

    private IBuzzFragmentView iForgotFragmentView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.buzz_fragment, container, false);
        iForgotFragmentView = (IBuzzFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iForgotFragmentView.setListener(new IBuzzFragmentView.Listener() {
            @Override
            public void back() {
                getActivity().finish();
            }

        });
    }


    private void startSignInFragment() {
        SignInFragment signInFragment = SignInFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, signInFragment, SignInFragment.TAG)
                .commit();

    }


}
