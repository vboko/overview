package xyz.gateguard.tablettenant.presentation.features.login.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.bus.event.ErrorEvent;
import xyz.gateguard.tablettenant.data.model.Forgot;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.BaseFragment;
import xyz.gateguard.tablettenant.presentation.features.language.activity.LanguageActivity;
import xyz.gateguard.tablettenant.presentation.features.login.fragmentview.IForgotFragmentView;

public class ForgotFragment extends BaseFragment {

    public static final String TAG = ForgotFragment.class.getSimpleName();

    public static ForgotFragment newInstance() {
        return new ForgotFragment();
    }

    private IForgotFragmentView iForgotFragmentView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.forgot_fragment, container, false);
        iForgotFragmentView = (IForgotFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iForgotFragmentView.setListener(new IForgotFragmentView.Listener() {
            @Override
            public void openSignIn() {
                startSignInFragment();
            }

            @Override
            public void changeLang() {
                LanguageActivity.startActivity(getActivity());
            }

            @Override
            public void forgot(String email, String phone) {
                forgotRequest(email, phone);
            }
        });
    }

    private void forgotRequest(String email, String phone) {
        showProgressDialog(getActivity());
        ApiController.forgot(email, phone);
    }


    private void startSignInFragment() {
        SignInFragment signInFragment = SignInFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, signInFragment, SignInFragment.TAG)
                .commit();
    }

    @Subscribe
    public void onForgotEvent(Forgot forgot) {
        if (forgot.getError() == null) {
            dismissProgressDialog();
            startSignInFragment();
        }
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent) {
        dismissProgressDialog();
        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }


}
