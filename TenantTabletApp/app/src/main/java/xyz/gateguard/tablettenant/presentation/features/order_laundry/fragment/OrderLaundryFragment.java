package xyz.gateguard.tablettenant.presentation.features.order_laundry.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseSupportFragment;
import xyz.gateguard.tablettenant.presentation.features.links.activity.LinksActivity;
import xyz.gateguard.tablettenant.presentation.features.order_laundry.fragmentview.IOrderLaundryFragmentView;
import xyz.gateguard.tablettenant.presentation.features.order_laundry.fragmentview.OrderLaundryFragmentView;

public class OrderLaundryFragment extends BaseSupportFragment {

    public static final String TAG = OrderLaundryFragment.class.getSimpleName();

    private OrderLaundryFragmentView schedulePickupFragmentView;


    public static OrderLaundryFragment newInstance() {
        return new OrderLaundryFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.order_laundry_fragment_view, container, false);
        schedulePickupFragmentView = (OrderLaundryFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        schedulePickupFragmentView.setListener(new IOrderLaundryFragmentView.Listener() {
            @Override
            public void back() {
                getActivity().onBackPressed();
            }

            @Override
            public void onUpsClick() {
                LinksActivity.startActivity(getActivity(), "https://www.google.com");
            }

            @Override
            public void onFedexClick() {
                LinksActivity.startActivity(getActivity(), "https://www.google.com");
            }

            @Override
            public void onDhlClick() {
                LinksActivity.startActivity(getActivity(), "https://www.google.com");
            }

            @Override
            public void onLaundryLimoClick() {

            }

            @Override
            public void onDryCleanExpressClick() {

            }

            @Override
            public void onCleanlyClick() {

            }

            @Override
            public void onAmazonClick() {

            }

            @Override
            public void onMacysClick() {

            }

            @Override
            public void onCostcoClick() {

            }

            @Override
            public void onLennysClick() {

            }

            @Override
            public void onStreetShoeRepairClick() {

            }

            @Override
            public void onQuestMedicalClick() {

            }
        });
    }
}
