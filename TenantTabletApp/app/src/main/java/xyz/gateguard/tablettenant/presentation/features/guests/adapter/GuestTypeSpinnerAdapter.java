package xyz.gateguard.tablettenant.presentation.features.guests.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.widget.ArrayAdapter;

public class GuestTypeSpinnerAdapter extends ArrayAdapter {

    String[] values;

    public GuestTypeSpinnerAdapter(Context context, int resource, String[] values) {
        super(context, resource);
        this.values = values;
    }

    @Nullable
    @Override
    public Object getItem(int position) {
        return values[position];
    }

    @Override
    public int getCount() {
        return values.length;
    }

}
