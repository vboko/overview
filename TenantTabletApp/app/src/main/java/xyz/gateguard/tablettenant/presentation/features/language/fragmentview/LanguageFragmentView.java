package xyz.gateguard.tablettenant.presentation.features.language.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.IView;
import xyz.gateguard.tablettenant.util.Localization;


public class LanguageFragmentView extends LinearLayout implements IView, ILanguageFragmentView {

    private Listener listener;
    private AppCompatImageView btnBack;
    LinearLayout lLangEn, lLangRu;
    String locale = "";
    private RelativeLayout btnSaveLanguage;

    public LanguageFragmentView(Context context) {
        super(context);
    }

    public LanguageFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LanguageFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {
        btnBack = (AppCompatImageView) findViewById(R.id.btnBack);
        btnBack.setVisibility(VISIBLE);
        btnSaveLanguage = (RelativeLayout) findViewById(R.id.save_language);
        lLangEn = (LinearLayout) findViewById(R.id.lLangEn);
        lLangRu = (LinearLayout) findViewById(R.id.lLangRu);

        setLocaleBackground();
    }

    private void setLocaleBackground() {
        switch (Localization.getLocale().getLanguage()) {
            case "ru":
                lLangRu.setBackgroundResource(R.drawable.shadow_accent_33);
                break;
            case "en":
                lLangEn.setBackgroundResource(R.drawable.shadow_accent_33);
                break;
            default:
                lLangEn.setBackgroundResource(R.drawable.shadow_accent_33);
                break;
        }
    }

    @Override
    public void setListeners() {
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.back();
            }
        });
        lLangEn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                locale = "en";
                clearAllLang();
                v.setBackgroundResource(R.drawable.shadow_accent_33);
            }
        });
        lLangRu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                locale = "ru";
                clearAllLang();
                v.setBackgroundResource(R.drawable.shadow_accent_33);
            }
        });
        btnSaveLanguage.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!locale.isEmpty()) {
                    listener.updateLocale(locale);
                }
            }
        });
    }

    private void clearAllLang() {
        lLangEn.setBackgroundResource(R.drawable.shadow_dark_33);
        lLangRu.setBackgroundResource(R.drawable.shadow_dark_33);
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
