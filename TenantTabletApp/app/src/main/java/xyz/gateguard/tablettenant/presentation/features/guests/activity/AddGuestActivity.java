package xyz.gateguard.tablettenant.presentation.features.guests.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.guests.fragment.AddGuestFragment;


public class AddGuestActivity extends BaseActivity {

    public static void startActivity(Context activity) {
        Intent intent = new Intent(activity, AddGuestActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));
        setCurrentLocale();
        startAddGuestFragment();
    }

    private void startAddGuestFragment() {
        AddGuestFragment addGuestFragment = (AddGuestFragment) getFragmentManager().findFragmentByTag(AddGuestFragment.TAG);
        if (addGuestFragment == null) {
            addGuestFragment = AddGuestFragment.newInstance();
            getFragmentManager().beginTransaction()
                    .replace(R.id.empty_frame_layout, addGuestFragment, AddGuestFragment.TAG)
                    .commit();
        }
    }
}
