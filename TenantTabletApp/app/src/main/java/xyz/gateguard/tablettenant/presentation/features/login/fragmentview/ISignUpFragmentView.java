package xyz.gateguard.tablettenant.presentation.features.login.fragmentview;

public interface ISignUpFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void createUser(String first_name, String last_name, String phone, String email, Integer root_user_id, String pp_user_id);
        void signIn();
        void changeLang();
    }

}

