package xyz.gateguard.tablettenant.presentation.features.guests.fragmentview;

import java.util.ArrayList;

public interface IAddGuestFragmentView {

    void setListener(Listener listener);

    void populateTime(boolean isFromTime, String value);

    void populateDate(boolean isFromDate, String value);

    interface Listener {
        void back();

        void onFromDateClick();

        void onToDateClick();

        void onFromTimeClick();

        void onToTimeClick();

        void onSendInviteCodeClick(String first_name, String last_name, String phone,
                                   String email, String date_from, String date_to,
                                   String time_from, String time_to, String guest_type, ArrayList<String> days);
    }
}
