package xyz.gateguard.tablettenant.presentation.features.account.fragment;


import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.gateguard.tablettenant.MyApplication;
import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.model.AddGuest;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.BaseSupportFragment;
import xyz.gateguard.tablettenant.presentation.features.account.activity.AddUnitActivity;
import xyz.gateguard.tablettenant.presentation.features.account.fragmentview.AccountFragmentView;
import xyz.gateguard.tablettenant.presentation.features.account.fragmentview.IAccountFragmentView;
import xyz.gateguard.tablettenant.presentation.features.language.activity.LanguageActivity;
import xyz.gateguard.tablettenant.presentation.features.splash.SplashScreenActivity;
import xyz.gateguard.tablettenant.presentation.features.tools.activity.ToolsServicesActivity;
import xyz.gateguard.tablettenant.util.MyPrefs;

import static xyz.gateguard.tablettenant.data.network.ApiController.sendErrorEvent;


public class AccountFragment extends BaseSupportFragment {

    public static final String TAG = AccountFragment.class.getSimpleName();

    private AccountFragmentView accountFragmentView;

    public static AccountFragment newInstance() {
        return new AccountFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.account_fragment_view, container, false);
        accountFragmentView = (AccountFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        accountFragmentView.setListener(new IAccountFragmentView.Listener() {
            @Override
            public void back() {
                ToolsServicesActivity.startActivity(getActivity());
                getActivity().finish();
            }

            @Override
            public void logout() {
                showLogoutDialog();
            }


            @Override
            public void onAddUnitClick() {
                AddUnitActivity.startActivity(getActivity());
            }

            @Override
            public void onEditUnitClick() {

            }

            @Override
            public void onChangePasClick() {
            }

            @Override
            public void onChangePinClick() {
            }

            @Override
            public void onChangeEmailClick() {

            }

            @Override
            public void onChangeLangClick() {
                LanguageActivity.startActivity(getActivity());
            }

            @Override
            public void onUpdatePaymentInfoClick() {

            }

            @Override
            public void onUpdateShippingAccountClick() {

            }

            @Override
            public void onSelectLanguageClick() {

            }
        });
    }


    private void showLogoutDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(R.string.dialog_logout)
                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        removeData();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // User cancelled the dialog
                    }
                });
        // Create the AlertDialog object and return it
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void removeData() {
        showProgressDialog(getActivity());
        Call<AddGuest> call = ApiController.getApiService().removeToken(MyPrefs.getDeviceId(), MyPrefs.getPushToken(MyApplication.getContext()));
        call.enqueue(new Callback<AddGuest>() {

            @Override
            public void onResponse(Call<AddGuest> call, Response<AddGuest> response) {
                dismissProgressDialog();
                MyPrefs.clearUserData();
                Intent intent = new Intent(getActivity(),SplashScreenActivity.class);
//                startActivity(intent);
                getActivity().finish();
//                restart(100);
            }

            @Override
            public void onFailure(Call<AddGuest> call, Throwable t) {
                dismissProgressDialog();
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public void restart(int delay) {
        PendingIntent intent = PendingIntent.getActivity(getActivity().getBaseContext(), 0, new Intent(getActivity(), SplashScreenActivity.class), PendingIntent.FLAG_CANCEL_CURRENT);
        AlarmManager manager = (AlarmManager) getActivity().getSystemService(Context.ALARM_SERVICE);
        manager.set(AlarmManager.RTC, System.currentTimeMillis() + delay, intent);
        System.exit(2);
    }

}
