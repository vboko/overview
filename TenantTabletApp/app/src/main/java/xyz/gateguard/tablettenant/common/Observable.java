package xyz.gateguard.tablettenant.common;

public interface Observable<T> {
	void registerObserver(T observer);
	void unregisterObserver(T observer);
}
