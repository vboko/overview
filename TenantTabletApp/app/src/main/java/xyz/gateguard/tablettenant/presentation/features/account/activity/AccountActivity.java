package xyz.gateguard.tablettenant.presentation.features.account.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.account.fragment.AccountFragment;
import xyz.gateguard.tablettenant.presentation.features.tools.activity.ToolsServicesActivity;


public class AccountActivity extends BaseActivity {

    public static void startActivity(Context activity) {
        Intent intent = new Intent(activity, AccountActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));
        setCurrentLocale();
        startToolsServicesFragment();
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        ToolsServicesActivity.startActivity(this);
        finish();
    }

    private void startToolsServicesFragment() {
        AccountFragment accountFragment = (AccountFragment) getSupportFragmentManager().findFragmentByTag(AccountFragment.TAG);
        if (accountFragment == null) {
            accountFragment = AccountFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.empty_frame_layout, accountFragment, AccountFragment.TAG)
                    .commit();
        }
    }
}
