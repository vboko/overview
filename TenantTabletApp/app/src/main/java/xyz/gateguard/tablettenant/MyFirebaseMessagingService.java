package xyz.gateguard.tablettenant;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

import xyz.gateguard.tablettenant.presentation.features.buzz.activity.BuzzActivity;

import static android.media.RingtoneManager.getDefaultUri;

/**
 * Created by Boichuk Dmitriy on 29.06.2016.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Map<String, String> map = remoteMessage.getData();

        for (String key : map.keySet()) {
            System.out.println("key : " + key);
            System.out.println("value : " + map.get(key));
        }

        Log.d(TAG, "From: " + remoteMessage.getFrom());
        sendNotification(map.get("hash"));

    }


    private void sendNotification(String hash) {
        int requestID = (int) System.currentTimeMillis();

        String messageBody = getResources().getString(R.string.hi_ari_someone_is_buzzing_you);
        Intent intent = new Intent(this, BuzzActivity.class);
        intent.putExtra("hash", hash);
        intent.putExtra("buzz", true);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);

        PendingIntent pendingIntent = PendingIntent.getActivity(this, requestID /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);

//        Uri sound = Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.not_sound);
        Uri sound = getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setSound(sound)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.cancelAll();

        notificationManager.notify((int) System.currentTimeMillis() /* ID of notification */, notificationBuilder.build());
    }
}