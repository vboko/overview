package xyz.gateguard.tablettenant.util;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;

import java.util.Locale;

import xyz.gateguard.tablettenant.MyApplication;

/**
 * Created by Dell on 25.04.2017.
 */

public class Localization {


    public static void setLocale(String lang, Context context) {
        Locale myLocale = new Locale(lang);
        Resources res = context.getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);

        MyApplication.getInstance().onConfigurationChanged(conf);
    }

    public static Locale getLocale() {
        return Locale.getDefault();

    }
}
