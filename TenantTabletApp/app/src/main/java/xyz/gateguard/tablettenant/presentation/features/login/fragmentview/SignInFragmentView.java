package xyz.gateguard.tablettenant.presentation.features.login.fragmentview;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.IView;

public class SignInFragmentView extends LinearLayout implements IView, ISignInFragmentView {

    private Listener listener;
    private TextView btnSignIn;
    private EditText eName, ePass;
    private LinearLayout btnLogin, btnForgot, btnLoginWith;
    private ImageView homeActivityLanguages, btnSignClick;
    private TextView tvForgot, tvLoginWith, tvEnter;
    private boolean isEmailLogin = false;

    public SignInFragmentView(Context context) {
        super(context);
    }

    public SignInFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SignInFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {
        btnSignIn = (TextView) findViewById(R.id.btnSignIn);
        findViewById(R.id.lBtnSignIn).setVisibility(GONE);
        btnSignIn.setText(R.string.sign_up);


        eName = (EditText) findViewById(R.id.e_username);
        ePass = (EditText) findViewById(R.id.e_password);
        btnLogin = (LinearLayout) findViewById(R.id.btn_login);
        homeActivityLanguages = (ImageView) findViewById(R.id.home_activity_languages);
        btnSignClick = (ImageView) findViewById(R.id.btnSignInClick);
        btnForgot = (LinearLayout) findViewById(R.id.btn_forgot);
        btnLoginWith = (LinearLayout) findViewById(R.id.btn_login_with);
        tvForgot = (TextView) findViewById(R.id.tv_forgot);
        tvLoginWith = (TextView) findViewById(R.id.tv_login_with);
        tvEnter = (TextView) findViewById(R.id.tv_enter_data);
        setIdEnterData();

    }



    @Override
    public void setListeners() {
        homeActivityLanguages.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.changeLang();
            }
        });
        btnSignClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.signUp();
            }
        });
        btnLoginWith.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (btnLoginWith.getTag().toString().equals("0")) {
                    btnLoginWith.setTag("1");
                    setIdEnterData();
                    isEmailLogin = false;
                } else {
                    btnLoginWith.setTag("0");
                    setUserNameData();
                    isEmailLogin = true;
                }
            }
        });
        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateData()) {

                    listener.loginNext(eName.getText().toString(), ePass.getText().toString(), isEmailLogin);
                } else {
                    ApiController.sendErrorEvent(getResources().getString(R.string.error_validate_data));
                }
            }
        });
        btnForgot.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.forgot();
            }
        });
    }

    private boolean validateData() {
        if (check(eName.getText().toString()) || check(ePass.getText().toString())) {
            return false;
        }
        return true;
    }

    private boolean check(String field) {
        if (field.length() < 1)
            return true;
        return false;
    }

    public void setIdEnterData() {
        btnLoginWith.setTag("1");
        eName.setHint(R.string.id_code);
        eName.setText("");
        eName.setInputType(InputType.TYPE_CLASS_NUMBER);
        ePass.setInputType(InputType.TYPE_CLASS_NUMBER);
        ePass.setText("");
        ePass.setHint(R.string.pin);
        tvEnter.setText(R.string.enter_your_id_and_pin);
        tvLoginWith.setText(R.string.login_with_username_amp_password);
    }

    public void setUserNameData() {
        btnLoginWith.setTag("0");
        eName.setHint(R.string.hint_username);
        eName.setText("");
        eName.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        ePass.setInputType(InputType.TYPE_CLASS_TEXT);
        ePass.setText("");
        ePass.setHint(R.string.hint_password);
        tvEnter.setText(R.string.enter_your_username_and_password);
        tvLoginWith.setText(R.string.login_with_id_code_amp_pin);
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
