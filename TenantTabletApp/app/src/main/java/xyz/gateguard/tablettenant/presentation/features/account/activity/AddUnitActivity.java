package xyz.gateguard.tablettenant.presentation.features.account.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.account.fragment.AddUnitFragment;


public class AddUnitActivity extends BaseActivity {

    public static void startActivity(Context activity) {
        Intent intent = new Intent(activity, AddUnitActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));
        setCurrentLocale();
        startAddUnitFragment();
    }

    private void startAddUnitFragment() {
        AddUnitFragment addUnitFragment = AddUnitFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, addUnitFragment, AddUnitFragment.TAG)
                .commit();

    }
}
