package xyz.gateguard.tablettenant.presentation.features.tools.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseSupportFragment;
import xyz.gateguard.tablettenant.presentation.features.account.activity.AccountActivity;
import xyz.gateguard.tablettenant.presentation.features.guests.activity.AddGuestActivity;
import xyz.gateguard.tablettenant.presentation.features.guests.activity.EditGuestsActivity;
import xyz.gateguard.tablettenant.presentation.features.order_laundry.activity.OrderLaundryActivity;
import xyz.gateguard.tablettenant.presentation.features.schedulepickup.activity.SchedulePickupActivity;
import xyz.gateguard.tablettenant.presentation.features.tools.fragmentview.IToolsServicesFragmentView;
import xyz.gateguard.tablettenant.presentation.features.tools.fragmentview.ToolsServicesFragmentView;


public class ToolsServicesFragment extends BaseSupportFragment {

    public static final String TAG = ToolsServicesFragment.class.getSimpleName();

    private ToolsServicesFragmentView toolsServicesFragmentView;

    public static ToolsServicesFragment newInstance() {
        return new ToolsServicesFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.tools_services_fragment_view, container, false);
        toolsServicesFragmentView = (ToolsServicesFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        toolsServicesFragmentView.setListener(new IToolsServicesFragmentView.Listener() {
            @Override
            public void back() {
                getActivity().onBackPressed();
            }

            @Override
            public void buzz() {
//                 BuzzActivity.startActivity(getActivity());
            }

            @Override
            public void onAddGuestClick() {
                AddGuestActivity.startActivity(getActivity());
            }

            @Override
            public void onEditGuestClick() {
                EditGuestsActivity.startActivity(getActivity());
            }

            @Override
            public void onSchedulePickupClick() {
                SchedulePickupActivity.startActivity(getActivity());
            }

            @Override
            public void onOrderLaundryClick() {
                OrderLaundryActivity.startActivity(getActivity());
            }

            @Override
            public void onOrderCleaningClick() {

            }

            @Override
            public void onSendLockedCodeClick() {

            }

            @Override
            public void onUpdateMyAccountClick() {
                AccountActivity.startActivity(getActivity());
                getActivity().finish();
            }

            @Override
            public void onUpdateMyPaymentInfoClick() {

            }
        });
    }
}
