package xyz.gateguard.tablettenant.presentation.features.guests.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.IView;
import xyz.gateguard.tablettenant.presentation.features.guests.adapter.NothingSelectedSpinnerAdapter;


public class AddGuestFragmentView extends LinearLayout implements IAddGuestFragmentView, IView {

    private EditText firstName, lastName;
    private EditText ePhoneNumber, eEmail;
    private TextView fromDate, toDate, fromTime, toTime;
    private Spinner guestType;
    private LinearLayout sendInviteCode, daySelector;
    private Listener listener;
    private AppCompatImageView btnBack;
    private AppCompatCheckBox cbNoPhoneEmail;
    private ArrayList<String> days = new ArrayList<>();

    public AddGuestFragmentView(Context context) {
        super(context);
    }

    public AddGuestFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AddGuestFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
        initGuestTypeSpinner();
    }

    @Override
    public void findViews() {
        firstName = (EditText) findViewById(R.id.add_guest_first_name);
        lastName = (EditText) findViewById(R.id.add_guest_last_name);
        ePhoneNumber = (EditText) findViewById(R.id.add_guest_mobile_number);
//        ePhoneNumber.addTextChangedListener(new PhoneNumberFormattingTextWatcher());

        eEmail = (EditText) findViewById(R.id.add_guest_email);
        fromDate = (TextView) findViewById(R.id.add_guest_from_date);
        toDate = (TextView) findViewById(R.id.add_guest_to_date);
        fromTime = (TextView) findViewById(R.id.add_guest_from_time);
        toTime = (TextView) findViewById(R.id.add_guest_to_time);
        sendInviteCode = (LinearLayout) findViewById(R.id.add_guest_send_invite_code);
        daySelector = (LinearLayout) findViewById(R.id.add_guest_day_selector);
        btnBack = (AppCompatImageView) findViewById(R.id.btnBack);
        guestType = (Spinner) findViewById(R.id.add_guest_guest_type);
        cbNoPhoneEmail = (AppCompatCheckBox) findViewById(R.id.cbNoPhoneEmail);


    }

    TextWatcher textWatcher = new TextWatcher() {
        private boolean mFormatting; // this is a flag which prevents the  stack overflow.
        private int mAfter;

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            // nothing to do here..
        }

        //called before the text is changed...
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            //nothing to do here...
            mAfter = after; // flag to detect backspace..

        }

        @Override
        public void afterTextChanged(Editable s) {
            // Make sure to ignore calls to afterTextChanged caused by the work done below
            if (!mFormatting) {
                mFormatting = true;
                // using US or RU formatting...
                if (mAfter != 0) // in case back space ain't clicked...
                {
                    String num = s.toString();
                    String data = null;
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        data = PhoneNumberUtils.formatNumber(num, "RU");
                    }


                    if (data != null) {
                        s.clear();
                        s.append(data);
                        Log.i("Number", data);//8 (999) 123-45-67 or +7 999 123-45-67
                    }

                }
                mFormatting = false;
            }
        }
    };

    private void initGuestTypeSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.guest_types, R.layout.contact_spinner_row_nothing_selected);
        adapter.setDropDownViewResource(R.layout.contact_spinner_row_nothing_selected);


        guestType.setAdapter(
                new NothingSelectedSpinnerAdapter(
                        adapter,
                        R.layout.contact_spinner_row_nothing_selected,
                        // R.layout.contact_spinner_nothing_selected_dropdown, // Optional
                        getContext()));

//        String[] guestTypes = getResources().getStringArray(R.array.guest_types);
//        GuestTypeSpinnerAdapter guestTypeSpinnerAdapter = new GuestTypeSpinnerAdapter(getContext(), R.layout.add_guest_type_spinner_item, guestTypes);
//        guestType.setAdapter(guestTypeSpinnerAdapter);
    }

    @Override
    public void setListeners() {
        cbNoPhoneEmail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ePhoneNumber.setEnabled(false);
                    eEmail.setEnabled(false);
                } else {
                    ePhoneNumber.setEnabled(true);
                    eEmail.setEnabled(true);

                }
            }
        });
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.back();
                }
            }
        });
        fromDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onFromDateClick();
                }
            }
        });

        toDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onToDateClick();
                }
            }
        });

        fromTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onFromTimeClick();
                }
            }
        });

        toTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onToTimeClick();
                }
            }
        });

        sendInviteCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {

                    if (validateData()) {
                        String ePhone = ePhoneNumber.getText().toString();
                        String eEmailt = eEmail.getText().toString();
                        String first_name = firstName.getText().toString(),
                                last_name = lastName.getText().toString(),
                                date_from = fromDate.getText().toString(),
                                date_to = toDate.getText().toString(),
                                time_from = fromTime.getText().toString(),
                                time_to = toTime.getText().toString(),
                                guest_type = guestType.getSelectedItemPosition()-1 + "";
                        listener.onSendInviteCodeClick(first_name, last_name, ePhone, eEmailt, date_from, date_to, time_from, time_to, guest_type, days);
                    } else {
                        ApiController.sendErrorEvent(getResources().getString(R.string.error_validate_data));
                    }
                }
            }
        });

        setDaySelectorListener();

    }

    private boolean validateData() {
        if (firstName.getText().toString().length() < 1)
            return false;
        if (lastName.getText().toString().length() < 1)
            return false;
        if (cbNoPhoneEmail.isChecked() == false) {
            String ePhone = ePhoneNumber.getText().toString();
            String eEmailt = eEmail.getText().toString();

            if (ePhone.length() < 6 || eEmailt.length() < 6)
                return false;
        }
        if (days.size() == 0) {
            return false;
        }
        if (guestType.getSelectedItemPosition()-1 < 0) {
            return false;
        }


        return true;
    }

    private void setDaySelectorListener() {
        for (int i = 0; i < daySelector.getChildCount(); i++) {
            final Button button = (Button) daySelector.getChildAt(i);
            final int j = i;
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    // invalidateDaySelectorButtons();
                    if (j != daySelector.getChildCount() - 1) {
                        if (view.getTag().toString().equals("0")) {
                            selectDay(button);
                        } else {
                            unSelectDay(button);
                            unselectAllButton();
                        }
                    } else {
                        if (view.getTag().toString().equals("0")) {
                            selectAllDays();
                        } else {
                            unSelectAllDays();
                        }
                    }
                }
            });
        }
    }

    private void unselectAllButton() {
        unSelectDay((Button) daySelector.getChildAt(daySelector.getChildCount() - 1));
    }

    private void unSelectAllDays() {
        for (int i = 0; i < daySelector.getChildCount(); i++) {
            unSelectDay((Button) daySelector.getChildAt(i));
        }
    }

    private void selectAllDays() {
        for (int i = 0; i < daySelector.getChildCount(); i++) {
            selectDay((Button) daySelector.getChildAt(i));
        }
    }

    private void invalidateDaySelectorButtons() {
        for (int i = 0; i < daySelector.getChildCount(); i++) {
            Button button = (Button) daySelector.getChildAt(i);
            button.setBackgroundColor(getResources().getColor(R.color.gray_light));
            button.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private String getStr(int res){
        return getResources().getString(res);
    }
    private void selectDay(Button button) {
        days.add(button.getText().toString());
        button.setTag("1");
        button.setTextColor(getResources().getColor(R.color.color_primary));
        button.setBackgroundColor(getResources().getColor(R.color.white));
    }

    private void unSelectDay(Button button) {
        days.remove(button.getText().toString());
        button.setTag("0");
        button.setBackgroundColor(getResources().getColor(R.color.gray_light));
        button.setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void populateTime(boolean isFromTime, String value) {
        if (isFromTime) {
            fromTime.setText(value);
        } else {
            toTime.setText(value);
        }
    }

    @Override
    public void populateDate(boolean isFromDate, String value) {
        if (isFromDate) {
            fromDate.setText(value);
        } else {
            toDate.setText(value);
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
