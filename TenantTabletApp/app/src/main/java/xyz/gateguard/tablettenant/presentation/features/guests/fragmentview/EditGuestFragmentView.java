package xyz.gateguard.tablettenant.presentation.features.guests.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.model.Guest;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.IView;
import xyz.gateguard.tablettenant.presentation.features.guests.adapter.NothingSelectedSpinnerAdapter;


public class EditGuestFragmentView extends LinearLayout implements IEditGuestFragmentView, IView {

    private EditText firstName, lastName;
    private EditText ePhoneNumber, eEmail;

    private TextView fromDate, toDate, fromTime, toTime;
    private Spinner guestType;
    private LinearLayout sendInviteCode, daySelector, removeGuest, resendInviteCode;
    private Listener listener;
    private AppCompatImageView btnBack;
    private AppCompatCheckBox cbNoPhoneEmail;
    private ArrayList<String> days = new ArrayList<>();

    public EditGuestFragmentView(Context context) {
        super(context);
    }

    public EditGuestFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public EditGuestFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
        initGuestTypeSpinner();
    }

    @Override
    public void findViews() {
        firstName = (EditText) findViewById(R.id.add_guest_first_name);
        lastName = (EditText) findViewById(R.id.add_guest_last_name);
        ePhoneNumber = (EditText) findViewById(R.id.add_guest_mobile_number);

        eEmail = (EditText) findViewById(R.id.add_guest_email);
        fromDate = (TextView) findViewById(R.id.add_guest_from_date);
        toDate = (TextView) findViewById(R.id.add_guest_to_date);
        fromTime = (TextView) findViewById(R.id.add_guest_from_time);
        toTime = (TextView) findViewById(R.id.add_guest_to_time);
        sendInviteCode = (LinearLayout) findViewById(R.id.add_guest_save_changes);
        daySelector = (LinearLayout) findViewById(R.id.add_guest_day_selector);
        removeGuest = (LinearLayout) findViewById(R.id.add_guest_remove_guest);
        resendInviteCode = (LinearLayout) findViewById(R.id.add_guest_send_invite_code);
        guestType = (Spinner) findViewById(R.id.add_guest_guest_type);
        btnBack = (AppCompatImageView) findViewById(R.id.btnBack);
        cbNoPhoneEmail = (AppCompatCheckBox) findViewById(R.id.cbNoPhoneEmail);

    }

    public void setData(Guest guest) {
        firstName.setText(guest.getGuestData().getFirstName());
        firstName.setText(guest.getGuestData().getFirstName());
        lastName.setText(guest.getGuestData().getLastName());
        eEmail.setText(guest.getGuestData().getEmail());
        ePhoneNumber.setText(guest.getGuestData().getMobilePhone());
        if (guest.getRestrictions() != null) {
            Guest.Restrictions restrictions = guest.getRestrictions();
            if (restrictions.getType() != null) {
                int type = Integer.parseInt(restrictions.getType());
                guestType.setSelection(type + 1);
            }
            populateTime(true, restrictions.getTimeStart());
            populateTime(false, restrictions.getTimeStart());
            populateDate(true, restrictions.getDateStart());
            populateDate(false, restrictions.getDateEnd());
//            for (Integer integer: restrictions.getDays()){
//                switch (integer){
//                    case 0:
//                        se
//                        break;
//                }
//            }
            for (int i = 0; i < daySelector.getChildCount(); i++) {
                final Button button = (Button) daySelector.getChildAt(i);
                try {
                    if (restrictions.getDays().contains(i))
                        selectDay(button);
                } catch (Exception e) {

                }

            }

        }
    }

    private void initGuestTypeSpinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(getContext(), R.array.guest_types, R.layout.contact_spinner_row_nothing_selected);
        adapter.setDropDownViewResource(R.layout.contact_spinner_row_nothing_selected);


        guestType.setAdapter(
                new NothingSelectedSpinnerAdapter(
                        adapter,
                        R.layout.contact_spinner_row_nothing_selected,
                        getContext()));
    }

    @Override
    public void setListeners() {
        cbNoPhoneEmail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ePhoneNumber.setEnabled(false);
                    eEmail.setEnabled(false);
                } else {
                    ePhoneNumber.setEnabled(true);
                    eEmail.setEnabled(true);

                }
            }
        });
        resendInviteCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.resendInvite();
            }
        });

        removeGuest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.remove();
                }
            }
        });

        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.back();
                }
            }
        });
        fromDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onFromDateClick();
                }
            }
        });

        toDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onToDateClick();
                }
            }
        });

        fromTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onFromTimeClick();
                }
            }
        });

        toTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onToTimeClick();
                }
            }
        });

        sendInviteCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    if (validateData()) {
                        String ePhone = ePhoneNumber.getText().toString();
                        String eEmailt = eEmail.getText().toString();
                        String first_name = firstName.getText().toString(),
                                last_name = lastName.getText().toString(),
                                date_from = fromDate.getText().toString(),
                                date_to = toDate.getText().toString(),
                                time_from = fromTime.getText().toString(),
                                time_to = toTime.getText().toString(),
                                guest_type = guestType.getSelectedItemPosition() - 1 + "";
                        listener.onSendInviteCodeClick(first_name, last_name, ePhone, eEmailt, date_from, date_to, time_from, time_to, guest_type, days);
                    } else {
                        ApiController.sendErrorEvent(getResources().getString(R.string.error_validate_data));
                    }
                }
            }
        });

        setDaySelectorListener();

    }

    private boolean validateData() {
        if (firstName.getText().toString().length() < 1)
            return false;
        if (lastName.getText().toString().length() < 1)
            return false;
        if (cbNoPhoneEmail.isChecked() == false) {
            String ePhone = ePhoneNumber.getText().toString();
            String eEmailt = eEmail.getText().toString();

            if (ePhone.length() < 6 || eEmailt.length() < 6)
                return false;
        }
        if (days.size() == 0) {
            return false;
        }
        if (guestType.getSelectedItemPosition() - 1 < 0) {
            return false;
        }


        return true;
    }

    private void setDaySelectorListener() {
        for (int i = 0; i < daySelector.getChildCount(); i++) {
            final Button button = (Button) daySelector.getChildAt(i);
            final int j = i;
            button.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View view) {
                    // invalidateDaySelectorButtons();
                    if (j != daySelector.getChildCount() - 1) {
                        if (view.getTag().toString().equals("0")) {
                            selectDay(button);
                        } else {
                            unSelectDay(button);
                            unselectAllButton();
                        }
                    } else {
                        if (view.getTag().toString().equals("0")) {
                            selectAllDays();
                        } else {
                            unSelectAllDays();
                        }
                    }
                }
            });
        }
    }

    private void unselectAllButton() {
        unSelectDay((Button) daySelector.getChildAt(daySelector.getChildCount() - 1));
    }

    private void unSelectAllDays() {
        for (int i = 0; i < daySelector.getChildCount(); i++) {
            unSelectDay((Button) daySelector.getChildAt(i));
        }
    }

    private void selectAllDays() {
        for (int i = 0; i < daySelector.getChildCount(); i++) {
            selectDay((Button) daySelector.getChildAt(i));
        }
    }

    private void invalidateDaySelectorButtons() {
        for (int i = 0; i < daySelector.getChildCount(); i++) {
            Button button = (Button) daySelector.getChildAt(i);
            button.setBackgroundColor(getResources().getColor(R.color.gray_light));
            button.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private void selectDay(Button button) {
        days.add(button.getText().toString());

        button.setTag("1");
        button.setTextColor(getResources().getColor(R.color.color_primary));
        button.setBackgroundColor(getResources().getColor(R.color.white));
    }

    private void unSelectDay(Button button) {
        days.remove(button.getText().toString());

        button.setTag("0");
        button.setBackgroundColor(getResources().getColor(R.color.gray_light));
        button.setTextColor(getResources().getColor(R.color.white));
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public void populateTime(boolean isFromTime, String value) {
        if (isFromTime) {
            fromTime.setText(value);
        } else {
            toTime.setText(value);
        }
    }

    @Override
    public void populateDate(boolean isFromDate, String value) {
        if (isFromDate) {
            fromDate.setText(value);
        } else {
            toDate.setText(value);
        }
    }

}
