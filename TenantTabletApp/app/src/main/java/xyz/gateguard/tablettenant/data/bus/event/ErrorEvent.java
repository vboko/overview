package xyz.gateguard.tablettenant.data.bus.event;


public class ErrorEvent {
    private String errorMessage;

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }
}
