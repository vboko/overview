package xyz.gateguard.tablettenant.presentation.features.account.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.IView;


public class AccountFragmentView extends LinearLayout implements IView, IAccountFragmentView {

    private LinearLayout addGuest, editGuest, schedulePickup, orderLaundry, orderCleaning, updateMyAccount, updateMyPaymentInfo,tools_services_select_language;
    private Listener listener;
    private AppCompatImageView btnBack,btnLogout;

    public AccountFragmentView(Context context) {
        super(context);
    }

    public AccountFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AccountFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {
        addGuest = (LinearLayout) findViewById(R.id.tools_services_add_guest);
        editGuest = (LinearLayout) findViewById(R.id.tools_services_edit_guest);
        schedulePickup = (LinearLayout) findViewById(R.id.tools_services_schedule_pickup);
        orderLaundry = (LinearLayout) findViewById(R.id.tools_services_order_laundry_services);
        orderCleaning = (LinearLayout) findViewById(R.id.tools_services_order_cleaning_services);
        updateMyAccount = (LinearLayout) findViewById(R.id.tools_services_update_my_account);
        updateMyPaymentInfo = (LinearLayout) findViewById(R.id.tools_services_update_my_payment_info);
        tools_services_select_language = (LinearLayout) findViewById(R.id.tools_services_select_language);
        btnBack = (AppCompatImageView) findViewById(R.id.btnBack);
        btnLogout = (AppCompatImageView) findViewById(R.id.btnLogout);
        btnBack.setVisibility(VISIBLE);
        btnLogout.setVisibility(VISIBLE);
    }

    @Override
    public void setListeners() {
        tools_services_select_language.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onChangeLangClick();
            }
        });
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.back();
            }
        });
        addGuest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onAddUnitClick();
                }
            }
        });

        editGuest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onEditUnitClick();
                }
            }
        });
        schedulePickup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onChangePasClick();
                }
            }
        });
        orderLaundry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onChangePinClick();
                }
            }
        });
        orderCleaning.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onChangeEmailClick();
                }
            }
        });

        updateMyAccount.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onUpdateShippingAccountClick();
                }
            }
        });
        btnLogout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.logout();
                }
            }
        });
        updateMyPaymentInfo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onSelectLanguageClick();
                }
            }
        });
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
