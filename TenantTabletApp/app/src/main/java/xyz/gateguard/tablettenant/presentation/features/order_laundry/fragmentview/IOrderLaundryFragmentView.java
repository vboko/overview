package xyz.gateguard.tablettenant.presentation.features.order_laundry.fragmentview;

public interface IOrderLaundryFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void back();
        void onUpsClick();

        void onFedexClick();

        void onDhlClick();

        void onLaundryLimoClick();

        void onDryCleanExpressClick();

        void onCleanlyClick();

        void onAmazonClick();

        void onMacysClick();

        void onCostcoClick();

        void onLennysClick();

        void onStreetShoeRepairClick();

        void onQuestMedicalClick();
    }
}
