package xyz.gateguard.tablettenant.presentation.features.account.fragmentview;

public interface IAddUnitFragmentView {

    void setListener(Listener listener);


    interface Listener {
        void back();
        void onFromDateClick();

        void onToDateClick();

        void onFromTimeClick();

        void onToTimeClick();

        void onSendInviteCodeClick();
    }
}
