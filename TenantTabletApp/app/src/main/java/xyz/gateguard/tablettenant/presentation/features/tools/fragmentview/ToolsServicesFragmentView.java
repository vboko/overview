package xyz.gateguard.tablettenant.presentation.features.tools.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.IView;


public class ToolsServicesFragmentView extends LinearLayout implements IView, IToolsServicesFragmentView {

    private LinearLayout addGuest, editGuest, schedulePickup, orderLaundry, orderCleaning, sendLockedCode, updateMyAccount, updateMyPaymentInfo;
    private Listener listener;
    private AppCompatImageView btnBack, btnBuzz;

    public ToolsServicesFragmentView(Context context) {
        super(context);
    }

    public ToolsServicesFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ToolsServicesFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {
        addGuest = (LinearLayout) findViewById(R.id.tools_services_add_guest);
        editGuest = (LinearLayout) findViewById(R.id.tools_services_edit_guest);
        schedulePickup = (LinearLayout) findViewById(R.id.tools_services_schedule_pickup);
        orderLaundry = (LinearLayout) findViewById(R.id.tools_services_order_laundry_services);
        orderCleaning = (LinearLayout) findViewById(R.id.tools_services_order_cleaning_services);
        sendLockedCode = (LinearLayout) findViewById(R.id.tools_services_send_a_locked_code);
        updateMyAccount = (LinearLayout) findViewById(R.id.tools_services_update_my_account);
        updateMyPaymentInfo = (LinearLayout) findViewById(R.id.tools_services_update_my_payment_info);
        btnBack = (AppCompatImageView) findViewById(R.id.btnBack);
        btnBuzz = (AppCompatImageView) findViewById(R.id.btnBuzz);
    }

    @Override
    public void setListeners() {
        btnBuzz.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.buzz();
            }
        });
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.back();
            }
        });
        addGuest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onAddGuestClick();
                }
            }
        });

        editGuest.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onEditGuestClick();
                }
            }
        });
        schedulePickup.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onSchedulePickupClick();
                }
            }
        });
        orderLaundry.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onOrderLaundryClick();
                }
            }
        });
        orderCleaning.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onOrderCleaningClick();
                }
            }
        });
        sendLockedCode.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onSendLockedCodeClick();
                }
            }
        });
        updateMyAccount.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onUpdateMyAccountClick();
                }
            }
        });
        updateMyPaymentInfo.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onUpdateMyPaymentInfoClick();
                }
            }
        });
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
