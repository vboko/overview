package xyz.gateguard.tablettenant.presentation.features.boot;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by Dmitriy Boichuk on 26.06.2017.
 */

public class CheckInetService extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

//        if (isNetworkAvailable(context)) {
//            Log.d("CallService", "isNetworkAvailable true");
//            SharedPrefsHelper sharedPrefsHelper;
//            sharedPrefsHelper = SharedPrefsHelper.getInstance();
//            if (sharedPrefsHelper.hasQbUser()) {
//                CallService.start(context, sharedPrefsHelper.getQbUser(), false);
//            }
//        } else
//            Log.d("CallService", "isNetworkAvailable false");
    }

    public boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

}