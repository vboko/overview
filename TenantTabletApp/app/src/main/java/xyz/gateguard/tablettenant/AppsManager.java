package xyz.gateguard.tablettenant;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;

import java.util.List;

import xyz.gateguard.tablettenant.common.Observable;
import xyz.gateguard.tablettenant.common.ObservableValue;
import xyz.gateguard.tablettenant.common.ValueObserver;

import static android.content.Context.MODE_PRIVATE;

public class AppsManager {
	public static final String APP_PREF = "app_pref";
    private static final String TAG = "AppsManager";
    private static AppsManager sInstance;

	private final Context mContext;
	private final DevicePolicyManager mDevicePolicyManager;
	private final ComponentName mAdminComponent;
	private final ObservableValue<State> mStateObservable = new ObservableValue<>(State.IDLE, true);
	private List<ApplicationInfo> mAppsList = Lists.newArrayList();
	public static void createInstance(final Context context) {
		sInstance = new AppsManager(context);
	}


	public static AppsManager getInstance() {
		return sInstance;
	}

	private AppsManager(final Context context) {
		mContext = context;
		mDevicePolicyManager = (DevicePolicyManager) context.getSystemService(Context.DEVICE_POLICY_SERVICE);
		mAdminComponent = AdministrationModeManager.getInstance().getAdminComponent();
		SharedPreferences prefs = context.getSharedPreferences(APP_PREF, MODE_PRIVATE);
	}

	public void reloadApps() {
		if (mStateObservable.getValue() != State.IDLE) {
			return;
		}

		mStateObservable.setValue(State.LOADING);

		new LoadingTask().execute();
	}

	public void showApp(final ApplicationInfo app) {
		if (mStateObservable.getValue() != State.IDLE) {
			return;
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			mDevicePolicyManager.setApplicationHidden(mAdminComponent, app.packageName, false);
		}
	}

	public void hideApp(String packageName) {
		if (mStateObservable.getValue() != State.IDLE) {
			return;
		}

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			mDevicePolicyManager.setApplicationHidden(mAdminComponent, packageName, true);
		}
	}

	public void unHideApp(String packageName){
		mDevicePolicyManager.setApplicationHidden(mAdminComponent, packageName, false);
	}



	public boolean isAppHidden(final ApplicationInfo app) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
			return mDevicePolicyManager.isApplicationHidden(mAdminComponent, app.packageName);
		}
		return false;
	}

	public Observable<ValueObserver<State>> getStateObservable() {
		return mStateObservable;
	}

	public ImmutableList<ApplicationInfo> getAppsList() {
		return ImmutableList.copyOf(mAppsList);
	}

	private class LoadingTask extends AsyncTask<Void, Void, List<ApplicationInfo>> {
		@Override
		protected List<ApplicationInfo> doInBackground(final Void... params) {
			final PackageManager packageManager = mContext.getPackageManager();
			return packageManager.getInstalledApplications(
					PackageManager.GET_META_DATA | PackageManager.GET_UNINSTALLED_PACKAGES
			);
		}

		@Override
		protected void onPostExecute(final List<ApplicationInfo> result) {
			if (result != null) {
				mAppsList = result;
			} else {
				mAppsList = Lists.newArrayList();
			}

			mStateObservable.setValue(State.IDLE);
		}
	}

	public enum State {
		IDLE, LOADING
	}


	public boolean isDeviceSecured() {
		return isAdminActive();
	}

	public boolean isAdminActive() {
		return mDevicePolicyManager.isAdminActive(mAdminComponent);
	}

	public boolean isOwner(){
		return mDevicePolicyManager.isDeviceOwnerApp(mContext.getApplicationContext().getPackageName());
	}

    public ComponentName getAdminComponent() {
        Log.d(TAG, "getAdminComponent ");
        return mAdminComponent;
    }


    /**
     * Through the PolicyAdmin receiver, the app can use this to trap various device
     * administration events, such as password change, incorrect password entry, etc.
     *
     */
//    public static class PolicyAdmin extends DeviceAdminReceiver {
//
//        @Override
//        public void onDisabled(Context context, Intent intent) {
//            // Called when the app is about to be deactivated as a device administrator.
//            // Deletes previously stored password policy.
//            super.onDisabled(context, intent);
//            SharedPreferences prefs = context.getSharedPreferences(APP_PREF, Activity.MODE_PRIVATE);
//            prefs.edit().clear().commit();
//        }
//
//		@Override
//		public void onEnabled(Context context, Intent intent) {
//			super.onEnabled(context, intent);
//
//
//		}
//	}
}
