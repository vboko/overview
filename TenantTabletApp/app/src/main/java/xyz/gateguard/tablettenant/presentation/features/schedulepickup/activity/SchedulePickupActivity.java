package xyz.gateguard.tablettenant.presentation.features.schedulepickup.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.schedulepickup.fragment.SchedulePickupFragment;

public class SchedulePickupActivity extends BaseActivity {

    public static void startActivity(FragmentActivity activity) {
        Intent intent = new Intent(activity, SchedulePickupActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));

        startScheduleFragment();
    }

    private void startScheduleFragment() {
        SchedulePickupFragment schedulePickupFragment = (SchedulePickupFragment) getSupportFragmentManager().findFragmentByTag(SchedulePickupFragment.TAG);
        if (schedulePickupFragment == null) {
            schedulePickupFragment = SchedulePickupFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.empty_frame_layout, schedulePickupFragment, SchedulePickupFragment.TAG)
                    .commit();
        }
    }

}
