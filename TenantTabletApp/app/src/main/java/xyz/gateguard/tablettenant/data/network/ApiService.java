package xyz.gateguard.tablettenant.data.network;

import com.google.gson.JsonObject;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import xyz.gateguard.tablettenant.data.model.AddGuest;
import xyz.gateguard.tablettenant.data.model.CreateUser;
import xyz.gateguard.tablettenant.data.model.Forgot;
import xyz.gateguard.tablettenant.data.model.Guest;
import xyz.gateguard.tablettenant.data.model.GuestList;
import xyz.gateguard.tablettenant.data.model.RemoveGuest;
import xyz.gateguard.tablettenant.data.model.ResendInvite;
import xyz.gateguard.tablettenant.data.model.SignInUser;


public interface ApiService {

    @FormUrlEncoded
    @POST("users")
//register
    Call<CreateUser> createUser(@Field("email") String email,
                                @Field("title") String title,
                                @Field("first_name") String first_name,
                                @Field("middle_name") String middle_name,
                                @Field("last_name") String last_name,
                                @Field("suffix") String suffix,
                                @Field("date_of_birth") String date_of_birth,
                                @Field("co_tenant") Boolean co_tenant,
                                @Field("root_relation") String root_relation,
                                @Field("root_user_id") Integer root_user_id,
                                @Field("pp_user_id") String pp_user_id);

    //    @FormUrlEncoded
    @POST("users/login-by-email")
    Call<SignInUser> signInEmail(@Body JsonObject data/*@Field(value = "email", encoded = true) String email,
                                 @Field("password") String password*/);

    @FormUrlEncoded
    @POST("users/login-by-pin")
    Call<SignInUser> signInPin(@Field(value = "id_code", encoded = true) String id_code,
                               @Field("pin") String pin, @Field("device_token") String device_token, @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST("users/forgot-password")
    Call<Forgot> forgot(@Field(value = "email", encoded = true) String email,
                        @Field("mobile_phone") String mobile_phone);

    @FormUrlEncoded
    @POST("devices/open")
    Call<Forgot> openDoor(@Field("hash") String hash);

    //    @FormUrlEncoded
    @POST("users/{user_id}/new-guests")
    Call<AddGuest> addGuest(@Path("user_id") String user_id,
                            @Body JsonObject restrictions);

    @POST("users/token")
    Call<AddGuest> setToken(@Body JsonObject data);

//    @DELETE("users/token")
    @FormUrlEncoded
    @HTTP(method = "DELETE", path = "users/token", hasBody = true)
    Call<AddGuest> removeToken(@Field("device_id") String device_id,
                               @Field("device_token") String device_token);


    @GET("users/{user_id}/new-guests")
    Call<GuestList> getGuests(@Path("user_id") String user_id);

    @GET("users/{user_id}/new-guests/{guest_id}")
    Call<Guest> getGuestByID(@Path("user_id") String user_id, @Path("guest_id") String guest_id);

    @DELETE("users/{user_id}/new-guests/{guest_id}")
    Call<RemoveGuest> removeGuest(@Path("user_id") String user_id, @Path("guest_id") String guest_id);

    @GET("resend-pin/{user_id}/{guest_id}")
    Call<ResendInvite> resendInviteGuest(@Path("user_id") String user_id, @Path("guest_id") String guest_id);

    //    @FormUrlEncoded
    @PUT("users/{user_id}/new-guests/{guest_id}")
    Call<AddGuest> editGuest(@Path("user_id") String user_id,
                             @Path("guest_id") String guest_id,
                             @Body JsonObject restrictions);
}
