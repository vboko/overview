package xyz.gateguard.tablettenant.presentation.features.language.fragment;


import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseSupportFragment;
import xyz.gateguard.tablettenant.presentation.features.language.fragmentview.ILanguageFragmentView;
import xyz.gateguard.tablettenant.presentation.features.language.fragmentview.LanguageFragmentView;
import xyz.gateguard.tablettenant.util.Localization;
import xyz.gateguard.tablettenant.util.MyPrefs;


public class LanguageFragment extends BaseSupportFragment {

    public static final String TAG = LanguageFragment.class.getSimpleName();

    private LanguageFragmentView languageFragmentView;

    public static LanguageFragment newInstance() {
        return new LanguageFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.language_fragment_view, container, false);
        languageFragmentView = (LanguageFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        languageFragmentView.setListener(new ILanguageFragmentView.Listener() {
            @Override
            public void back() {
                getActivity().onBackPressed();
            }


            @Override
            public void updateLocale(String locale) {
                Localization.setLocale(locale, getActivity());
                MyPrefs.setLang(getActivity(),locale);
//                getActivity().recreate();
                getActivity().finish();
            }


        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // refresh your views here
        Toast.makeText(getActivity(), "onConfigurationChanged LANG FRAGMENT", Toast.LENGTH_SHORT).show();
        super.onConfigurationChanged(newConfig);
    }
}
