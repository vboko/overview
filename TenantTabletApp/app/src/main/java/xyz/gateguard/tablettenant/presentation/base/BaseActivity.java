package xyz.gateguard.tablettenant.presentation.base;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.view.WindowManager;

import java.util.Locale;

import xyz.gateguard.tablettenant.MyApplication;
import xyz.gateguard.tablettenant.util.Localization;

public class BaseActivity extends AppCompatActivity {
    private static final String TAG = "BaseActivity";
    private Locale currentLocale;

    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    public void setCurrentLocale() {
//        currentLocale = Localization.getLocale();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        SimCardData simCardData = new SimCardData();
//        String ICCID = simCardData.getICCID(this);

        if (currentLocale != null) {
            if (currentLocale.getLanguage().equals(MyApplication.sDefSystemLanguage.getLanguage()) == false) {
                currentLocale = MyApplication.sDefSystemLanguage;
                recreate();
            } else {
            }
        } else {
            currentLocale = Localization.getLocale();
        }
    }
}
