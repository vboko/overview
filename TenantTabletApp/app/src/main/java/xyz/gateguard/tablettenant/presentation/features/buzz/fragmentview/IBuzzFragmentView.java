package xyz.gateguard.tablettenant.presentation.features.buzz.fragmentview;

public interface IBuzzFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void back();

    }

}
