package xyz.gateguard.tablettenant.data.model;

/**
 * Created by Dmitriy Boichuk on 14.05.2017.
 */

public class ClickItem {
    private String id;

    public ClickItem(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}
