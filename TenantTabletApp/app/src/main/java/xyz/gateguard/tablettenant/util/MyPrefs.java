package xyz.gateguard.tablettenant.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import xyz.gateguard.tablettenant.MyApplication;

/**
 * Created by Dell on 25.04.2017.
 */

public class MyPrefs {
    public static void setLang(Context context, String lang) {
        SharedPreferences preferences = context.getSharedPreferences("LANG",
                android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lang", lang);
        editor.commit();
    }

    public static String getLang(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("LANG",
                android.content.Context.MODE_PRIVATE);
        return preferences.getString("lang", "");
    }

    public static void setToken(Context context, String lang) {
        SharedPreferences preferences = context.getSharedPreferences("TOKEN",
                android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("token", lang);
        editor.commit();
    }

    public static String getToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("TOKEN",
                android.content.Context.MODE_PRIVATE);
        return preferences.getString("token", "");
    }

    public static void setPushToken(Context context, String lang) {
        SharedPreferences preferences = context.getSharedPreferences("PUSHTOKEN",
                android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("PUSHTOKEN", lang);
        editor.commit();
    }

    public static String getPushToken(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("PUSHTOKEN",
                android.content.Context.MODE_PRIVATE);
        return preferences.getString("PUSHTOKEN", "");
    }

    public static void setUserId(Context context, String user_id) {
        SharedPreferences preferences = context.getSharedPreferences("USER_ID",
                android.content.Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("user_id", user_id);
        editor.commit();
    }

    public static String getUserId(Context context) {
        SharedPreferences preferences = context.getSharedPreferences("USER_ID",
                android.content.Context.MODE_PRIVATE);
        return preferences.getString("user_id", "");
    }


    public static String getDeviceId() {
        return Settings.Secure.getString(MyApplication.getInstance().getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static void clearUserData() {
        setUserId(MyApplication.getContext(), "");
        setToken(MyApplication.getContext(), "");
        setPushToken(MyApplication.getContext(), "");
    }
}
