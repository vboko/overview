package xyz.gateguard.tablettenant.presentation.features.login.fragmentview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.IView;

public class ForgotFragmentView extends LinearLayout implements IView, IForgotFragmentView {

    private Listener listener;
    private TextView btnSignIn;
    private LinearLayout btnSendResLink;
    private ImageView btnSignClick, homeActivityLanguages;
    private EditText ePhone, eEmail;

    public ForgotFragmentView(Context context) {
        super(context);
    }

    public ForgotFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ForgotFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {
        btnSignIn = (TextView) findViewById(R.id.btnSignIn);
        findViewById(R.id.lBtnSignIn).setVisibility(VISIBLE);
        btnSignIn.setText(R.string.log_in);
        homeActivityLanguages = (ImageView) findViewById(R.id.home_activity_languages);
        ePhone = (EditText) findViewById(R.id.e_phone);
        eEmail = (EditText) findViewById(R.id.e_email);
        btnSendResLink = (LinearLayout) findViewById(R.id.btn_send_res_link);
        btnSignClick = (ImageView) findViewById(R.id.btnSignInClick);

    }


    @Override
    public void setListeners() {
        btnSignClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.openSignIn();
            }
        });
        btnSendResLink.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateData()) {
                    listener.forgot(eEmail.getText().toString(),ePhone.getText().toString());
                } else {
                    ApiController.sendErrorEvent(getResources().getString(R.string.error_validate_data));
                }
            }
        });
        homeActivityLanguages.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.changeLang();
            }
        });
    }

    private boolean validateData() {
        if (check(ePhone.getText().toString()) || check(eEmail.getText().toString())) {
            return false;
        }
        return true;
    }

    private boolean check(String field) {
        if (field.length() < 4)
            return true;
        return false;
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
