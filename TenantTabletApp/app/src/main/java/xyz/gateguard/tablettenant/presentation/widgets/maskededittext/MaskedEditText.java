package xyz.gateguard.tablettenant.presentation.widgets.maskededittext;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;

import com.vicmikhailau.maskededittext.IFormattedString;
import com.vicmikhailau.maskededittext.MaskedFormatter;
import com.vicmikhailau.maskededittext.MaskedWatcher;

public class MaskedEditText extends AppCompatEditText {

    // ===========================================================
    // Fields
    // ===========================================================

    private MaskedFormatter mMaskedFormatter;
    private MaskedWatcher mMaskedWatcher;

    // ===========================================================
    // Constructors
    // ===========================================================

    public MaskedEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, com.vicmikhailau.maskededittext.R.styleable.MaskedEditText);

        if (typedArray.hasValue(com.vicmikhailau.maskededittext.R.styleable.MaskedEditText_mask)) {
            String maskStr = typedArray.getString(com.vicmikhailau.maskededittext.R.styleable.MaskedEditText_mask);

            if (maskStr != null && !maskStr.isEmpty()) {
                setMask(maskStr);
            }
        }

        typedArray.recycle();
    }

    // ===========================================================
    // Getter & Setter
    // ===========================================================

    public String getMaskString() {
        return mMaskedFormatter.getMaskString();
    }

    public String getUnMaskedText() {
        String currentText = getText().toString();
        IFormattedString formattedString = mMaskedFormatter.formatString(currentText);
        return formattedString.getUnMaskedString();
    }

    public void setMask(String mMaskStr) {
        mMaskedFormatter = new MaskedFormatter(mMaskStr);

        if (mMaskedWatcher != null) {
            removeTextChangedListener(mMaskedWatcher);
        }

        mMaskedWatcher = new MaskedWatcher(mMaskedFormatter, this);
        addTextChangedListener(mMaskedWatcher);
    }

}
