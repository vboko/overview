package xyz.gateguard.tablettenant.presentation.features.login.activity;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;

import xyz.gateguard.tablettenant.AppsManager;
import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.common.ValueObserver;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.base.RegisterAdmin;
import xyz.gateguard.tablettenant.presentation.features.login.fragment.ForgotFragment;
import xyz.gateguard.tablettenant.presentation.features.login.fragment.SignInFragment;
import xyz.gateguard.tablettenant.presentation.features.login.fragment.SignUpFragment;
import xyz.gateguard.tablettenant.util.Localization;

public class SignInActivity extends BaseActivity {
    private static final String TAG = "SignInActivity";
    private String currentLocale;
    private ProgressBar mProgressBar;
    private AppsManager mAppsManager = AppsManager.getInstance();
    private AppsManagerStateObserver mAppsManagerStateObserver = new AppsManagerStateObserver();
    //    private PackageManager mPackageManager = getApplicationContext().getPackageManager();;
    private static final int REQ_ACTIVATE_DEVICE_ADMIN = 10;

    public static void startActivity(Context context) {
        Intent intent = new Intent(context, SignInActivity.class);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
        setCurrentLocale();
        currentLocale = Localization.getLocale().getLanguage();

//        hideSystemUI();

        mAppsManager.getStateObservable().registerObserver(mAppsManagerStateObserver);
        RegisterAdmin registerAdmin = new RegisterAdmin(getApplicationContext(), this);
        registerAdmin.activateAdmin();
    }

    private void getFragmentLogin() {
        Fragment fragment = (Fragment) getFragmentManager().findFragmentById(R.id.empty_frame_layout);
        if (fragment == null) {
            startSignInFragment();
        } else {
            if (fragment instanceof SignInFragment) {
                startSignInFragment();
            } else if (fragment instanceof ForgotFragment) {
                startForgotFragment();
            } else if (fragment instanceof SignUpFragment) {
                startSignUpFragment();
            }
        }
    }

    private void startSignInFragment() {
        SignInFragment signInFragment = SignInFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, signInFragment, SignInFragment.TAG)
                .commit();

    }

    private void startForgotFragment() {
        ForgotFragment forgotFragment = ForgotFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, forgotFragment, forgotFragment.TAG)
                .commit();

    }

    private void startSignUpFragment() {
        SignUpFragment signUpFragment = SignUpFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, signUpFragment, signUpFragment.TAG)
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFragmentLogin();
    }

//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            final View decorView = inflater.inflate(R.layout.fragment_main, container, false);
//
//            // Hide both the navigation bar and the status bar.
//            // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
//            // a general rule, you should design your app to hide the status bar whenever you
//            // hide the navigation bar.
//            final int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE;
//            decorView.setSystemUiVisibility(uiOptions);
//
//            decorView.setOnSystemUiVisibilityChangeListener
//                    (new View.OnSystemUiVisibilityChangeListener() {
//                        @Override
//                        public void onSystemUiVisibilityChange(int visibility) {
//
//                            Timer timer = new Timer();
//                            TimerTask task = new TimerTask() {
//                                @Override
//                                public void run() {
//                                    getActivity().runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            decorView.setSystemUiVisibility(uiOptions);
//                                            // Remember that you should never show the action bar if the
//                                            // status bar is hidden, so hide that too if necessary.
////                                            ActionBar actionBar = getActivity().getActionBar();
////                                            actionBar.hide();
//                                        }
//                                    });
//                                }
//                            };
//
//                            timer.scheduleAtFixedRate(task, 0, 1);
//                        }
//                    });
//
//            return decorView;
//        }
//    }

//    @Override
//    public void onWindowFocusChanged(boolean hasFocus) {
//        super.onWindowFocusChanged(hasFocus);
//        // When the window loses focus (e.g. the action overflow is shown),
//        // cancel any pending hide action. When the window gains focus,
//        // hide the system UI.
////        if (hasFocus) {
////            delayedHide(1);
////        } else {
//        mHideHandler.removeMessages(0);
////        }
//    }
//
//    private void hideSystemUI() {
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_FULLSCREEN
//                | View.SYSTEM_UI_FLAG_LOW_PROFILE
//                | View.SYSTEM_UI_FLAG_IMMERSIVE);
//    }
//
//    private void showSystemUI() {
//        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
//    }
//
//    private final Handler mHideHandler = new Handler() {
//        @Override
//        public void handleMessage(Message msg) {
//            hideSystemUI();
//        }
//    };
//
//    private void delayedHide(int delayMillis) {
//        mHideHandler.removeMessages(0);
//        mHideHandler.sendEmptyMessageDelayed(0, delayMillis);
//    }

    private class AppsManagerStateObserver implements ValueObserver<AppsManager.State> {
        @Override
        public void onChanged(final AppsManager.State value) {
            switch (value) {
                case IDLE:
                    Log.d(TAG, "apps list iddle");
                    break;

                case LOADING:
                    Log.d(TAG, "apps list loading");
                    break;
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mAppsManager != null) {
            mAppsManager.getStateObservable().unregisterObserver(mAppsManagerStateObserver);
        }
        mProgressBar = null;
    }
}
