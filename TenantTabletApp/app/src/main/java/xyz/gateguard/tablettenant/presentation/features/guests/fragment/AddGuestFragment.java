package xyz.gateguard.tablettenant.presentation.features.guests.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.otto.Subscribe;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;
import com.wdullaer.materialdatetimepicker.time.Timepoint;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.bus.event.ErrorEvent;
import xyz.gateguard.tablettenant.data.model.AddGuest;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.BaseFragment;
import xyz.gateguard.tablettenant.presentation.features.guests.fragmentview.AddGuestFragmentView;
import xyz.gateguard.tablettenant.presentation.features.guests.fragmentview.IAddGuestFragmentView;
import xyz.gateguard.tablettenant.util.CalendarUtils;


public class AddGuestFragment extends BaseFragment implements com.wdullaer.materialdatetimepicker.date.DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener {

    public static final String TAG = AddGuestFragment.class.getSimpleName();

    private AddGuestFragmentView addGuestFragmentView;
    private boolean isFormDate, isFromTime;

    public static AddGuestFragment newInstance() {
        return new AddGuestFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.add_guest_fragment_view, container, false);
        addGuestFragmentView = (AddGuestFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addGuestFragmentView.setListener(new IAddGuestFragmentView.Listener() {
            @Override
            public void back() {
                getActivity().onBackPressed();
            }

            @Override
            public void onFromDateClick() {
                isFormDate = true;
                showDatePickerDialog();
            }

            @Override
            public void onToDateClick() {
                isFormDate = false;
                showDatePickerDialog();
            }

            @Override
            public void onFromTimeClick() {
                isFromTime = true;
                showTimePickerDialog();
            }

            @Override
            public void onToTimeClick() {
                isFromTime = false;
                showTimePickerDialog();
            }

            @Override
            public void onSendInviteCodeClick(String first_name, String last_name, String phone, String email, String date_from, String date_to, String time_from, String time_to, String guest_type, ArrayList<String> days) {
                showProgressDialog(getActivity());
                ApiController.addGuest(first_name, last_name, phone, email, date_from, date_to, time_from, time_to, guest_type, days);
            }


        });

        Calendar calendar = Calendar.getInstance();
        int a = calendar.get(Calendar.AM_PM);
        String typeNow = "PM";
        String typeNext = "AM";
        if (a == Calendar.AM) {
            typeNow = "AM";
            typeNext = "PM";
        }
        addGuestFragmentView.populateTime(true, String.format("%s:%s %s", calendar.get(Calendar.HOUR), new Time(System.currentTimeMillis()).getMinutes(), typeNow));
        addGuestFragmentView.populateTime(false, String.format("%s:%s %s", calendar.get(Calendar.HOUR), new Time(System.currentTimeMillis()).getMinutes(), typeNext));

        Date date = new GregorianCalendar(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)).getTime();
        calendar.setTime(date);
        addGuestFragmentView.populateDate(true, CalendarUtils.getDate(calendar));
        calendar.add(Calendar.DATE, 1);
        addGuestFragmentView.populateDate(false, CalendarUtils.getDate(calendar));

    }

    private void showDatePickerDialog() {
        Calendar now = Calendar.getInstance();
        DatePickerDialog dpd = DatePickerDialog.newInstance(
                AddGuestFragment.this,
                now.get(Calendar.YEAR),
                now.get(Calendar.MONTH),
                now.get(Calendar.DAY_OF_MONTH)
        );
        dpd.setAccentColor(getResources().getColor(R.color.color_primary_dark));
        dpd.show(getFragmentManager(), "Datepickerdialog");
    }


    private void showTimePickerDialog() {
        Calendar now = Calendar.getInstance();
        TimePickerDialog dpd = TimePickerDialog.newInstance(
                AddGuestFragment.this,
                now.get(Calendar.HOUR),
                now.get(Calendar.MINUTE),
                false
        );
        dpd.setAccentColor(getResources().getColor(R.color.color_primary_dark));
//        dpd.setVersion(TimePickerDialog.Version.VERSION_2);
        dpd.show(getFragmentManager(), "Timepickerdialog");
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        //Toast.makeText(getActivity(), String.valueOf(year), Toast.LENGTH_LONG).show();
        Calendar calendar = Calendar.getInstance();
        Date date = new GregorianCalendar(year, monthOfYear, dayOfMonth).getTime();
        calendar.setTime(date);
        addGuestFragmentView.populateDate(isFormDate, CalendarUtils.getDate(calendar));
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        Timepoint timepoint = view.getSelectedTime();
        if (timepoint.isAM()) {
            addGuestFragmentView.populateTime(isFromTime, String.format("%s:%s %s", timepoint.getHour(), timepoint.getMinute(), "AM"));
        } else if (timepoint.isPM()) {
            int hour = timepoint.getHour();
            if (hour > 12) {
                hour = hour - 12;
            }
            addGuestFragmentView.populateTime(isFromTime, String.format("%s:%s %s", hour, timepoint.getMinute(), "PM"));
        }
    }

    @Subscribe
    public void onAddGuestEvent(AddGuest addGuest) {
        dismissProgressDialog();
        getActivity().finish();
        Toast.makeText(getActivity(), "DONE", Toast.LENGTH_SHORT).show();
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent) {
        dismissProgressDialog();
        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }
}
