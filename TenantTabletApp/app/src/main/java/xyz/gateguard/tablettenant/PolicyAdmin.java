package xyz.gateguard.tablettenant;

import android.app.Activity;
import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import static xyz.gateguard.tablettenant.AppsManager.APP_PREF;

/**
 * Created by D on 18/12/2017.
 */

public class PolicyAdmin extends DeviceAdminReceiver {

    /**
     * Through the PolicyAdmin receiver, the app can use this to trap various device
     * administration events, such as password change, incorrect password entry, etc.
     *
     */
        @Override
        public void onDisabled(Context context, Intent intent) {
            // Called when the app is about to be deactivated as a device administrator.
            // Deletes previously stored password policy.
            super.onDisabled(context, intent);
            SharedPreferences prefs = context.getSharedPreferences(APP_PREF, Activity.MODE_PRIVATE);
            prefs.edit().clear().commit();
        }

        @Override
        public void onEnabled(Context context, Intent intent) {
            super.onEnabled(context, intent);
    }
}
