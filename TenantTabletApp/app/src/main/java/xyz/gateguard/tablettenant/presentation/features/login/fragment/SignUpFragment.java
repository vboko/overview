package xyz.gateguard.tablettenant.presentation.features.login.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.bus.event.ErrorEvent;
import xyz.gateguard.tablettenant.data.model.CreateUser;
import xyz.gateguard.tablettenant.presentation.base.BaseFragment;
import xyz.gateguard.tablettenant.presentation.features.language.activity.LanguageActivity;
import xyz.gateguard.tablettenant.presentation.features.login.fragmentview.ISignUpFragmentView;

public class SignUpFragment extends BaseFragment {

    public static final String TAG = SignUpFragment.class.getSimpleName();

    public static SignUpFragment newInstance() {
        return new SignUpFragment();
    }

    private ISignUpFragmentView iSignUpFragmentView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sign_up_fragment, container, false);
        iSignUpFragmentView = (ISignUpFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iSignUpFragmentView.setListener(new ISignUpFragmentView.Listener() {
            @Override
            public void createUser(String first_name, String last_name, String phone, String email, Integer root_user_id, String pp_user_id) {
//                createUserReuest(first_name, last_name, phone, email, root_user_id, pp_user_id);
            }

            @Override
            public void signIn() {
                startSignInFragment();
            }

            @Override
            public void changeLang() {
                LanguageActivity.startActivity(getActivity());
            }
        });
    }



    @Subscribe
    public void onCreateUserEvent(CreateUser createUser) {
//        mData = companiesInfo.getData();
//        mCompanies.addAll(mData.getData());
//        mCompaniesAdapter.notifyDataSetChanged();
//
        dismissProgressDialog();
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent) {
        dismissProgressDialog();
        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }


    private void startSignInFragment() {
        SignInFragment signInFragment = SignInFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, signInFragment, SignInFragment.TAG)
                .commit();

    }

}
