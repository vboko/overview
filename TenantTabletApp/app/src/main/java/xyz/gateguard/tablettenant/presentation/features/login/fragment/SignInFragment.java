package xyz.gateguard.tablettenant.presentation.features.login.fragment;

import android.app.admin.DevicePolicyManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import java.util.List;

import xyz.gateguard.tablettenant.AppsManager;
import xyz.gateguard.tablettenant.Constants;
import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.bus.event.ErrorEvent;
import xyz.gateguard.tablettenant.data.model.SignInUser;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.BaseFragment;
import xyz.gateguard.tablettenant.presentation.features.language.activity.LanguageActivity;
import xyz.gateguard.tablettenant.presentation.features.login.fragmentview.ISignInFragmentView;
import xyz.gateguard.tablettenant.presentation.features.tools.activity.ToolsServicesActivity;
import xyz.gateguard.tablettenant.util.MyPrefs;

public class SignInFragment extends BaseFragment {

    public static final String TAG = SignInFragment.class.getSimpleName();

    public static SignInFragment newInstance() {
        return new SignInFragment();
    }

    private ISignInFragmentView iSignInFragmentView;
    private DevicePolicyManager mDpm;
    private AppsManager mAppsManager = AppsManager.getInstance();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.sign_in_fragment, container, false);
        iSignInFragmentView = (ISignInFragmentView) rootView;

        final int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_FULLSCREEN;
        rootView.setSystemUiVisibility(uiOptions);

//        rootView.setOnSystemUiVisibilityChangeListener
//                (new View.OnSystemUiVisibilityChangeListener() {
//                    @Override
//                    public void onSystemUiVisibilityChange(int visibility) {
//
//                        Timer timer = new Timer();
//                        TimerTask task = new TimerTask() {
//                            @Override
//                            public void run() {
//                                getActivity().runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        rootView.setSystemUiVisibility(uiOptions);
//                                        // Remember that you should never show the action bar if the
//                                        // status bar is hidden, so hide that too if necessary.
////                                        ActionBar actionBar = getActivity().getActionBar();
////                                        actionBar.hide();
//                                    }
//                                });
//                            }
//                        };
//
//                        timer.scheduleAtFixedRate(task, 0, 1);
//                    }
//                });

        return rootView;
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        iSignInFragmentView.setListener(new ISignInFragmentView.Listener() {
            @Override
            public void changeLang() {
                LanguageActivity.startActivity(getActivity());
            }

            @Override
            public void signUp() {
                startSignUpFragment();
            }

            @Override
            public void loginNext(String login, String pass, boolean isEmail) {
                signIn(login, pass, isEmail);
            }

//            @Override
//            public void loginNext() {
//                ToolsServicesActivity.startActivity(getActivity());
//                getActivity().finish();
//            }

            @Override
            public void forgot() {
                startForgotFragment();
            }


        });
    }

    private void signIn(String login, String pass, boolean isEmail) {
        showProgressDialog(getActivity());
        mDpm = (DevicePolicyManager) getActivity().getSystemService(Context.DEVICE_POLICY_SERVICE);

        //if owner was set  deleting Gategaurd is not possible, only by hard reset. This command allow to remove owner
        if (login.equals("1111") && pass.equals("1111")) {
            if (mAppsManager.isOwner()) {
                mDpm.clearDeviceOwnerApp(getActivity().getPackageName());
                dismissProgressDialog();
            } else {
                Toast.makeText(getActivity(), getString(R.string.not_device_owner), Toast.LENGTH_SHORT).show();
                dismissProgressDialog();
            }
        } else
            //unhide apps which was hiden
            if (login.equals("2222") && pass.equals("2222")) {
                unhideApps();
                dismissProgressDialog();
            } else {
                ApiController.signInUserEmail(login, pass, isEmail);
            }
    }

    private void unhideApps() {
        Log.d(TAG, "isAdminActive " + mAppsManager.isAdminActive());
        List<ApplicationInfo> appList = mAppsManager.getAppsList();
        for (ApplicationInfo app : appList) {
            //hide all apps except this app and launcher
//
            if (!app.packageName.equals(getActivity().getApplicationContext().getPackageName())
                    && !app.packageName.equals(Constants.PACKAGE_LAUNCHER) && !app.packageName.equals(Constants.PACKAGE_SETTINGS)
                    && !app.packageName.equals("com.android.systemui")
                    && !app.packageName.equals("com.android.packageinstaller")
                    && !app.packageName.equals(("android"))
                    && !app.packageName.equals(("com.example.android.apis"))
                    && !app.packageName.equals(("com.example.android.apiscom.android.wallpaper"))
                    && !app.packageName.equals("com.android.galaxy4")
                    && !app.packageName.equals("com.android.inputdevices")
                    && !app.packageName.equals("com.android.development")
                    && !app.packageName.equals("com.android.provision")
                    && !app.packageName.equals("com.android.certinstaller")
                    && !app.packageName.equals("com.example.android.livecubes")
                    && !app.packageName.equals("com.android.defcontainer")
                    && !app.packageName.equals("com.android.backupconfirm")
                    && !app.packageName.equals("com.amaze.filemanager")
                    && !app.packageName.equals("com.android.customlocale2")
                    && !app.packageName.equals("com.android.captiveportallogin")
                    && !app.packageName.equals("com.android.inputdevices")
                    && !app.packageName.equals("com.android.vpndialogs")
                    && !app.packageName.equals("com.android.inputmethod.latin")
                    && !app.packageName.equals("com.android.inputdevices")
                    && !app.packageName.equals("com.android.inputdevices")) {
                mAppsManager.unHideApp(app.packageName);
            }
        }
    }

    private void startForgotFragment() {
        ForgotFragment forgotFragment = ForgotFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, forgotFragment, forgotFragment.TAG)
                .commit();

    }

    private void startSignUpFragment() {
        SignUpFragment signUpFragment = SignUpFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, signUpFragment, signUpFragment.TAG)
                .commit();
    }

    @Subscribe
    public void onSignInUserEvent(SignInUser signInUser) {
        dismissProgressDialog();

        if (signInUser.getError() == null) {
            MyPrefs.setToken(getActivity(), signInUser.getToken());
            MyPrefs.setUserId(getActivity(), signInUser.getUser().getId());
            ApiController.recreateApiService();
            ToolsServicesActivity.startActivity(getActivity());
            getActivity().finish();
        }
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent) {
        dismissProgressDialog();
        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }


}
