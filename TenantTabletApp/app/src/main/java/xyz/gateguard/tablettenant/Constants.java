package xyz.gateguard.tablettenant;

/**
 * Created by D on 19/12/2017.
 */

public class Constants {
    public static final String PACKAGE_LAUNCHER = "com.android.launcher3";
    public static final String PACKAGE_SETTINGS = " com.android.settings";

    public static final String MOBILE_BLOCKING_NAME = "setMobileDataEnabled";
}
