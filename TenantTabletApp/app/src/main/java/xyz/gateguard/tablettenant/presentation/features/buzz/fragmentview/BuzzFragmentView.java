package xyz.gateguard.tablettenant.presentation.features.buzz.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.IView;

public class BuzzFragmentView extends LinearLayout implements IView, IBuzzFragmentView {

    private Listener listener;
    private AppCompatImageView btnBack;

    public BuzzFragmentView(Context context) {
        super(context);
    }

    public BuzzFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BuzzFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {
        btnBack = (AppCompatImageView) findViewById(R.id.btnBack);
    }


    @Override
    public void setListeners() {
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null)
                    listener.back();
            }
        });
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
