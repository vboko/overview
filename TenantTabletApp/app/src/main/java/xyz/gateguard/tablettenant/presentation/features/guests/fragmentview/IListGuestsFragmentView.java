package xyz.gateguard.tablettenant.presentation.features.guests.fragmentview;

public interface IListGuestsFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void back();

    }
}
