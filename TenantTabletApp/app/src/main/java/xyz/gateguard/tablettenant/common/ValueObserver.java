package xyz.gateguard.tablettenant.common;

public interface ValueObserver<T> {
	void onChanged(T value);
}
