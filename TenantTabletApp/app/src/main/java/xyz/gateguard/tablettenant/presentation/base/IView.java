package xyz.gateguard.tablettenant.presentation.base;

public interface IView {

    void findViews();

    void setListeners();
    }
