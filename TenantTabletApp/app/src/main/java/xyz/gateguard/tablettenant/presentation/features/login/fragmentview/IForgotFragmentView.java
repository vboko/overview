package xyz.gateguard.tablettenant.presentation.features.login.fragmentview;

public interface IForgotFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void openSignIn();
        void changeLang();
        void forgot(String email, String phone);

    }

}
