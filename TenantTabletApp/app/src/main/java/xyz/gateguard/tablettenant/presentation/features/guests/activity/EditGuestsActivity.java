package xyz.gateguard.tablettenant.presentation.features.guests.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.guests.fragment.EditGuestFragment;
import xyz.gateguard.tablettenant.presentation.features.guests.fragment.ListGuestsFragment;


public class EditGuestsActivity extends BaseActivity {

    public static void startActivity(Context activity) {
        Intent intent = new Intent(activity, EditGuestsActivity.class);
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));
        setCurrentLocale();
        startListGuestFragment();
//        startEdittGuestFragment("25");
    }

    private void startListGuestFragment() {
        ListGuestsFragment listGuestsFragment = ListGuestsFragment.newInstance();
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, listGuestsFragment, ListGuestsFragment.TAG)
//                .addToBackStack(null)
                .commit();
    }

    private void startEdittGuestFragment(String id) {
        EditGuestFragment editGuestFragment = EditGuestFragment.newInstance(id);
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, editGuestFragment, EditGuestFragment.TAG)
                .commit();
    }
}
