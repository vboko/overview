package xyz.gateguard.tablettenant;

import android.app.Application;
import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.content.res.Configuration;
import android.net.ConnectivityManager;
import android.os.Build;
import android.view.View;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Locale;

import xyz.gateguard.tablettenant.presentation.features.boot.CheckInetService;

/**
 * Created by Dell on 25.04.2017.
 */
public class MyApplication extends Application {

    private static final String TAG = "MyApp";
    public static /*String*/ Locale sDefSystemLanguage;
    private static MyApplication instance;
    private static Context mContext;
    private View mDecorView;
    private DevicePolicyManager mDpm;

    @Override
    public void onCreate() {
        super.onCreate();
        sDefSystemLanguage = Locale.getDefault();//.getLanguage();
        instance = this;
        mContext = this;
        registerNetworkBroadcastForNougat();

        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        String currentHomePackage = resolveInfo.activityInfo.packageName;


        AdministrationModeManager.createInstance(this);
        AppsManager.createInstance(this);
        AppsManager.getInstance().reloadApps();

        Process process = null;
        try {
            process = Runtime.getRuntime().exec("dpm set-device-owner xyz.gateguard.tablettenant/.PolicyAdmin");
            BufferedReader bufferedReader = new BufferedReader(
                    new InputStreamReader(process.getInputStream()));
            PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(process.getOutputStream())), true);
            process.waitFor();
            bufferedReader.close();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        try {
            ProcessBuilder pb = new ProcessBuilder(
                    "dpm set-device-owner xyz.gateguard.tablettenant/.PolicyAdmin");
            Process pc = pb.start();
            pc.waitFor();
            System.out.println("Done");
//            Toast.makeText(this, "dpm success", Toast.LENGTH_LONG).show();
        } catch (IOException e) {
//            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        } catch (InterruptedException e) {
//            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


        ComponentName deviceAdmin = new ComponentName(this, PolicyAdmin.class);
        mDpm = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        if (!mDpm.isAdminActive(deviceAdmin)) {
            Toast.makeText(this, getString(R.string.not_device_admin), Toast.LENGTH_SHORT).show();
        }

        if (mDpm.isDeviceOwnerApp(getPackageName())) {
            mDpm.setLockTaskPackages(deviceAdmin, new String[]{getPackageName()});
        } else {
            Toast.makeText(this, getString(R.string.not_device_owner), Toast.LENGTH_SHORT).show();
        }
    }


//    public void hideApps() {
//        if (AppsManager.getInstance().isAdminActive()) {
//            List<ApplicationInfo> appList = AppsManager.getInstance().getAppsList();
//            AppsManager.getInstance().hideApp("com.android.phone");
//            AppsManager.getInstance().hideApp("com.android.providers.calendar");
//            for (ApplicationInfo app : appList) {
//                //hide all apps except this app
//                if (!app.packageName.equals(mContext.getApplicationContext().getPackageName())
//                        || !app.packageName.equals(Constants.PACKAGE_LAUNCHER)) {
//                    AppsManager.getInstance().hideApp(app.packageName);
//                }
//            }

//        }
//    }

    private void registerNetworkBroadcastForNougat() {
        CheckInetService mNetworkReceiver = new CheckInetService();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            registerReceiver(mNetworkReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        }
    }

    public static MyApplication getInstance() {
        return instance;
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        sDefSystemLanguage = newConfig.locale;//.getLanguage();
    }

    public static Context getContext() {
        return mContext;
    }


    /**
     * A placeholder fragment containing a simple view.
     */
//    public static class PlaceholderFragment extends Fragment {
//
//        public PlaceholderFragment() {
//        }
//
//        @Override
//        public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                                 Bundle savedInstanceState) {
//            final View decorView = inflater.inflate(R.layout.fragment_main, container, false);
//
//            // Hide both the navigation bar and the status bar.
//            // SYSTEM_UI_FLAG_FULLSCREEN is only available on Android 4.1 and higher, but as
//            // a general rule, you should design your app to hide the status bar whenever you
//            // hide the navigation bar.
//            final int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
//                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_FULLSCREEN
//                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
//                    | View.SYSTEM_UI_FLAG_IMMERSIVE;
//            decorView.setSystemUiVisibility(uiOptions);
//
//            decorView.setOnSystemUiVisibilityChangeListener
//                    (new View.OnSystemUiVisibilityChangeListener() {
//                        @Override
//                        public void onSystemUiVisibilityChange(int visibility) {
//
//                            Timer timer = new Timer();
//                            TimerTask task = new TimerTask() {
//                                @Override
//                                public void run() {
//                                    getActivity().runOnUiThread(new Runnable() {
//                                        @Override
//                                        public void run() {
//                                            decorView.setSystemUiVisibility(uiOptions);
//                                            // Remember that you should never show the action bar if the
//                                            // status bar is hidden, so hide that too if necessary.
////                                            ActionBar actionBar = getActivity().getActionBar();
////                                            actionBar.hide();
//                                        }
//                                    });
//                                }
//                            };
//
//                            timer.scheduleAtFixedRate(task, 0, 1);
//                        }
//                    });
//
//            return decorView;
//        }
//    }

}