package xyz.gateguard.tablettenant.presentation.features.account.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.text.Html;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.IView;


public class AddUnitFragmentView extends LinearLayout implements IAddUnitFragmentView, IView {


    private Listener listener;
    private AppCompatImageView btnBack;
    private TextView tvWelcome, tvEnterPin, tvChooseNewPin, tvDoNotUse;

    public AddUnitFragmentView(Context context) {
        super(context);
    }

    public AddUnitFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public AddUnitFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {

        btnBack = (AppCompatImageView) findViewById(R.id.btnBack);
        btnBack.setVisibility(VISIBLE);
        tvWelcome = (TextView) findViewById(R.id.tvWelcome);
        tvEnterPin = (TextView) findViewById(R.id.tvEnterPin);
        tvChooseNewPin = (TextView) findViewById(R.id.tvChooseNewPin);
        tvDoNotUse = (TextView) findViewById(R.id.tvDoNotUse);
        setEnterPinTitle();
        settvDoNotUseTitle();
        setChoosePinTitle();
        setWelcomeName("ARI");
    }

    private void setWelcomeName(String name) {
        name = "<b>" + name.toUpperCase() + "</b>";
        String welcome = String.format(getContext().getString(R.string.welcome), name);
        tvWelcome.setText(Html.fromHtml(welcome));
    }

    private void setEnterPinTitle() {
        String pin = "<b>PIN</b>";
        String id = "<b>ID</b>";
        String welcome = String.format(getContext().getString(R.string.enter_the_pin_and_id), pin, id);
        tvEnterPin.setText(Html.fromHtml(welcome));
    }

    private void setChoosePinTitle() {
        String pin = "<b>"+getContext().getString(R.string.new_t)+"</b>";
        String welcome = String.format(getContext().getString(R.string.choose_a_new_pin), pin);
        tvChooseNewPin.setText(Html.fromHtml(welcome));
    }

    private void settvDoNotUseTitle() {
        String pin = "<b>"+getContext().getString(R.string.not)+"</b>";
        String welcome = String.format(getContext().getString(R.string.do_not_use_your_banking_pin), pin);
        tvDoNotUse.setText(Html.fromHtml(welcome));
    }

    @Override
    public void setListeners() {
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.back();
                }
            }
        });


    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

}
