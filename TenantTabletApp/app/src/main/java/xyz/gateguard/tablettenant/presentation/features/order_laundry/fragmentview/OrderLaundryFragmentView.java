package xyz.gateguard.tablettenant.presentation.features.order_laundry.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.IView;

public class OrderLaundryFragmentView extends LinearLayout implements IView, IOrderLaundryFragmentView {

    private AppCompatImageView ups, fedex, dhl, btnBack;
    private Listener listener;

    public OrderLaundryFragmentView(Context context) {
        super(context);
    }

    public OrderLaundryFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public OrderLaundryFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {
        btnBack = (AppCompatImageView) findViewById(R.id.btnBack);
        ups = (AppCompatImageView) findViewById(R.id.schedule_pickup_ups);
        fedex = (AppCompatImageView) findViewById(R.id.schedule_pickup_fedex);
        dhl = (AppCompatImageView) findViewById(R.id.schedule_pickup_dhl);

    }

    @Override
    public void setListeners() {
        btnBack.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.back();
            }
        });
        ups.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onUpsClick();
                }
            }
        });

        fedex.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onFedexClick();
                }
            }
        });

        dhl.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null) {
                    listener.onDhlClick();
                }
            }
        });


    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
