package xyz.gateguard.tablettenant.presentation.widgets;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;

import xyz.gateguard.tablettenant.R;

/**
 * Created by Dell on 29.04.2017.
 */

public class ProgressDialog {
    public static Dialog getProgressDialog(Context context) {
        Dialog progressDialog = new Dialog(context);
        progressDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        progressDialog.setContentView(R.layout.dialog_progress);
        progressDialog.setCancelable(false);
        return progressDialog;
    }
}
