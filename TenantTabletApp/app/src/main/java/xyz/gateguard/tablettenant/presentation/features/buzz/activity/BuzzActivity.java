package xyz.gateguard.tablettenant.presentation.features.buzz.activity;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.bus.BusProvider;
import xyz.gateguard.tablettenant.data.bus.event.ErrorEvent;
import xyz.gateguard.tablettenant.data.model.Forgot;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.widgets.ProgressDialog;

public class BuzzActivity extends BaseActivity implements View.OnClickListener {
    private String hash = "";
    private ImageView imgSelfie;
    private RelativeLayout btnReject, btnUnlock;


    private String SELFIE_URL = "http://device-api.gateguard.xyz/storage/call_device/device_%s.jpg?q=%s";

    private Dialog progressDialog;

    public void showProgressDialog(Context context) {
        if (progressDialog == null)
            progressDialog = ProgressDialog.getProgressDialog(context);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        Log.d("Log", "dismissProgressDialog");
        if (progressDialog != null)
            progressDialog.dismiss();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buzz_fragment);

        imgSelfie = (ImageView) findViewById(R.id.imgSelfie);
        btnReject = (RelativeLayout) findViewById(R.id.btnReject);
        btnUnlock = (RelativeLayout) findViewById(R.id.btnUnlock);

        btnReject.setOnClickListener(this);
        btnUnlock.setOnClickListener(this);

        if (getIntent().getStringExtra("hash") != null) {
            hash = getIntent().getStringExtra("hash");
            Picasso.with(this)
                    .load(String.format(SELFIE_URL, hash, System.currentTimeMillis() + ""))
                    .into(imgSelfie);
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnReject:
                finish();
                break;
            case R.id.btnUnlock:

            {
                showProgressDialog(this);
                Call<Forgot> call = ApiController.getApiService().openDoor(hash);
                call.enqueue(new Callback<Forgot>() {

                    @Override
                    public void onResponse(Call<Forgot> call, Response<Forgot> response) {
                        dismissProgressDialog();
                        if (response.isSuccessful()) {
                            Forgot object = response.body();
                            BusProvider.getInstance().post(object);
                        } else {
//                            Toast.makeText(BuzzActivity.this, R.string.error_message, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Forgot> call, Throwable t) {
                        dismissProgressDialog();
                        finish();
//                        Toast.makeText(BuzzActivity.this, R.string.error_network_message, Toast.LENGTH_SHORT).show();

                    }
                });
            }
//                showProgressDialog(this);
//                ApiController.openDoor(hash);
            break;
        }
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent) {
//        dismissProgressDialog();
//        finish();
//        Toast.makeText(this, errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

}
