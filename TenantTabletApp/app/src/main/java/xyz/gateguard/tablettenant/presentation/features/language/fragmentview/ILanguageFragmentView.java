package xyz.gateguard.tablettenant.presentation.features.language.fragmentview;

public interface ILanguageFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void back();


        void updateLocale(String locale);
    }
}
