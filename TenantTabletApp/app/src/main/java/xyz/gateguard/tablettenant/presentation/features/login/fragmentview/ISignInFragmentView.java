package xyz.gateguard.tablettenant.presentation.features.login.fragmentview;

public interface ISignInFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void changeLang();
        void signUp();
        void loginNext(String login, String pass, boolean isEmail);
        void forgot();
    }

}
