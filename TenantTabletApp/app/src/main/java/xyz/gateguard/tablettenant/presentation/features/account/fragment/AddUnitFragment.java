package xyz.gateguard.tablettenant.presentation.features.account.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseFragment;
import xyz.gateguard.tablettenant.presentation.features.account.fragmentview.AddUnitFragmentView;
import xyz.gateguard.tablettenant.presentation.features.account.fragmentview.IAddUnitFragmentView;


public class AddUnitFragment extends BaseFragment{

    public static final String TAG = AddUnitFragment.class.getSimpleName();

    private AddUnitFragmentView addGuestFragmentView;

    public static AddUnitFragment newInstance() {
        return new AddUnitFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.add_unit_fragment_view, container, false);
        addGuestFragmentView = (AddUnitFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addGuestFragmentView.setListener(new IAddUnitFragmentView.Listener() {
            @Override
            public void back() {
                getActivity().finish();

            }

            @Override
            public void onFromDateClick() {

            }

            @Override
            public void onToDateClick() {

            }

            @Override
            public void onFromTimeClick() {

            }

            @Override
            public void onToTimeClick() {

            }

            @Override
            public void onSendInviteCodeClick() {

            }
        });

    }

}
