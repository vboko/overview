package xyz.gateguard.tablettenant.presentation.base;

import android.app.Dialog;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import xyz.gateguard.tablettenant.presentation.widgets.ProgressDialog;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class BaseSupportFragment extends Fragment {

    public void replaceFragment(Fragment fragment, String tag, int idContainer) {
        getFragmentManager().beginTransaction()
                .replace(idContainer, fragment, tag)
                .addToBackStack(null)
                .commit();
    }

    public void replaceFragmentStateLoss(Fragment fragment, String tag, int idContainer) {
        getFragmentManager().beginTransaction()
                .replace(idContainer, fragment, tag)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }
    private Dialog progressDialog;

    public void showProgressDialog(Context context) {
        if (progressDialog == null)
            progressDialog = ProgressDialog.getProgressDialog(context);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        Log.d("Log","dismissProgressDialog");
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void onPause() {
        super.onPause();
        hideSoftKeyBoard();
    }
    private void hideSoftKeyBoard() {
        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);

        if(imm.isAcceptingText()) {
            try {
                imm.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
            }catch (Exception e){

            }
        }
    }
}
