package xyz.gateguard.tablettenant.presentation.features.guests.fragment;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.squareup.otto.Subscribe;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.bus.event.ErrorEvent;
import xyz.gateguard.tablettenant.data.model.ClickItem;
import xyz.gateguard.tablettenant.data.model.GuestList;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.BaseFragment;
import xyz.gateguard.tablettenant.presentation.features.guests.adapter.GuestListAdapter;
import xyz.gateguard.tablettenant.presentation.features.guests.fragmentview.IListGuestsFragmentView;
import xyz.gateguard.tablettenant.presentation.features.guests.fragmentview.ListGuestsFragmentView;


public class ListGuestsFragment extends BaseFragment {

    public static final String TAG = ListGuestsFragment.class.getSimpleName();

    private ListGuestsFragmentView listGuestsFragmentView;
    private RecyclerView rvGuest;

    public static ListGuestsFragment newInstance() {
        return new ListGuestsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_guests_fragment_view, container, false);
        listGuestsFragmentView = (ListGuestsFragmentView) rootView;
        rvGuest = (RecyclerView) rootView.findViewById(R.id.rvGuests);
        rvGuest.setHasFixedSize(true);
        rvGuest.setLayoutManager(new LinearLayoutManager(getActivity()));
//         textView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                startEdittGuestFragment();
//            }
//        });
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        showProgressDialog(getActivity());
        ApiController.getGuests();
    }

    private void startEdittGuestFragment(String id) {
        EditGuestFragment editGuestFragment = EditGuestFragment.newInstance(id);
        getFragmentManager().beginTransaction()
                .replace(R.id.empty_frame_layout, editGuestFragment, EditGuestFragment.TAG)
                .addToBackStack(null)
                .commit();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listGuestsFragmentView.setListener(new IListGuestsFragmentView.Listener() {
            @Override
            public void back() {
                getActivity().finish();
            }
        });

    }

    @Subscribe
    public void onClickItemEvent(ClickItem item) {
        startEdittGuestFragment(item.getId());
     }

    @Subscribe
    public void onGetGuestEvent(final GuestList guestList) {
        dismissProgressDialog();
        if (guestList.getData() != null) {
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    GuestListAdapter guestListAdapter = new GuestListAdapter(getActivity(), guestList.getData());

                    rvGuest.setAdapter(guestListAdapter);

                }
            }, 200);
        }
    }

    @Subscribe
    public void onErrorEvent(ErrorEvent errorEvent) {
        dismissProgressDialog();
        Toast.makeText(getActivity(), errorEvent.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }

}
