package xyz.gateguard.tablettenant.presentation.features.login.fragmentview;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.network.ApiController;
import xyz.gateguard.tablettenant.presentation.base.IView;

public class SignUpFragmentView extends LinearLayout implements IView, ISignUpFragmentView {

    private Listener listener;
    private TextView btnSignIn;
    private ImageView btnSignClick, homeActivityLanguages;
    private LinearLayout btnRegister;
    private EditText eFirstName, eLastName, ePhone, eEmail, eIdCode, ePin;
    private AppCompatCheckBox cbNoMobile, cbNoEmail, cbNoIdPin;

    public SignUpFragmentView(Context context) {
        super(context);
    }

    public SignUpFragmentView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SignUpFragmentView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        findViews();
        setListeners();
    }

    @Override
    public void findViews() {
        btnSignClick = (ImageView) findViewById(R.id.btnSignInClick);
        btnSignIn = (TextView) findViewById(R.id.btnSignIn);
        btnRegister = (LinearLayout) findViewById(R.id.btnRegister);
        findViewById(R.id.lBtnSignIn).setVisibility(VISIBLE);
        homeActivityLanguages = (ImageView) findViewById(R.id.home_activity_languages);

        btnSignIn.setText(R.string.login);

        eFirstName = (EditText) findViewById(R.id.e_first_name);
        eLastName = (EditText) findViewById(R.id.e_last_name);
        ePhone = (EditText) findViewById(R.id.e_phone);
        eEmail = (EditText) findViewById(R.id.e_email);
        eIdCode = (EditText) findViewById(R.id.e_id_code);
        ePin = (EditText) findViewById(R.id.e_pin);

        cbNoEmail = (AppCompatCheckBox) findViewById(R.id.cb_no_email);
        cbNoIdPin = (AppCompatCheckBox) findViewById(R.id.cb_no_id_pin);
        cbNoMobile = (AppCompatCheckBox) findViewById(R.id.cb_no_mobile);
    }


    @Override
    public void setListeners() {
        homeActivityLanguages.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.changeLang();
            }
        });
        btnSignClick.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.signIn();
            }
        });
        btnRegister.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cbNoEmail.isChecked() && cbNoEmail.isChecked() && cbNoIdPin.isChecked()) {
                    ApiController.sendErrorEvent(getResources().getString(R.string.enter_at_least_one_mobile_email_or_id));

                } else {
                    if (validateData()) {
                        listener.createUser("esf", "sdf", "dsf", "sdf", 1, "dsg");
                    } else {
                        ApiController.sendErrorEvent(getResources().getString(R.string.error_validate_data));
                    }
                }
            }
        });
    }

    private boolean validateData() {
        if (check(eFirstName.getText().toString())) {
            return false;
        }
        if (check(eLastName.getText().toString())) {
            return false;
        }
        if (!cbNoMobile.isChecked() && check(ePhone.getText().toString())) {
            return false;
        }
        if (!cbNoEmail.isChecked() && check(eEmail.getText().toString())) {
            return false;
        }
        if (!cbNoIdPin.isChecked() && check(ePin.getText().toString())) {
            return false;
        }


        return false;
    }

    private boolean check(String field) {
        if (field.length() < 4)
            return true;
        return false;
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }
}
