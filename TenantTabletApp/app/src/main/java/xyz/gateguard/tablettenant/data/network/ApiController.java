package xyz.gateguard.tablettenant.data.network;


import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

import java.io.IOException;
import java.util.ArrayList;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import xyz.gateguard.tablettenant.MyApplication;
import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.bus.BusProvider;
import xyz.gateguard.tablettenant.data.bus.event.ErrorEvent;
import xyz.gateguard.tablettenant.data.model.AddGuest;
import xyz.gateguard.tablettenant.data.model.Forgot;
import xyz.gateguard.tablettenant.data.model.Guest;
import xyz.gateguard.tablettenant.data.model.GuestList;
import xyz.gateguard.tablettenant.data.model.RemoveGuest;
import xyz.gateguard.tablettenant.data.model.ResendInvite;
import xyz.gateguard.tablettenant.data.model.SignInUser;
import xyz.gateguard.tablettenant.util.MyPrefs;
import xyz.gateguard.tablettenant.util.TextValues;

public class ApiController {
    private static ApiService apiService;

    public static ApiService getApiService() {
        if (apiService == null) {
            OkHttpClient okHttpClient = null;
            if (MyPrefs.getToken(MyApplication.getContext()).isEmpty()) {
                okHttpClient = defaultHttpClient;
            } else {
                okHttpClient = defaultWTokenHttpClient;
            }
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(ApiConstants.BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .build();

            apiService = retrofit.create(ApiService.class);
        }
        return apiService;
    }

    public static void recreateApiService() {
        apiService = null;
    }

    private static OkHttpClient defaultHttpClient = new OkHttpClient.Builder()
            .addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder()
                            .addHeader("Accept", "application/json").build();
                    return chain.proceed(request);
                }
            })
            .addInterceptor(getHttpLoggingInterceptor())
            .build();

    private static OkHttpClient defaultWTokenHttpClient = new OkHttpClient.Builder()
            .addInterceptor(new Interceptor() {
                @Override
                public okhttp3.Response intercept(Chain chain) throws IOException {
                    Request request = chain.request().newBuilder()
                            .addHeader("Authorization", "Bearer " + MyPrefs.getToken(MyApplication.getContext()))
                            .addHeader("Accept", "application/json").build();
                    return chain.proceed(request);
                }
            })
            .addInterceptor(getHttpLoggingInterceptor())
            .build();

    private static HttpLoggingInterceptor getHttpLoggingInterceptor() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
        return logging;
    }


    public static void signInUserEmail(String login, String password, boolean isEmail) {
        Call<SignInUser> call;
        if (isEmail) {
            JsonObject jsonObject = new JsonObject();
//            *@Field(value = "email", encoded = true) String email,
//            @Field("password") String password*/
            jsonObject.addProperty("email", login);
            jsonObject.addProperty("password", password);
            jsonObject.addProperty("device_token", MyPrefs.getPushToken(MyApplication.getContext()));
            jsonObject.addProperty("device_id", MyPrefs.getDeviceId());
            call = ApiController.getApiService().signInEmail(jsonObject);
        } else {
            call = ApiController.getApiService().signInPin(login, password, MyPrefs.getPushToken(MyApplication.getContext()), MyPrefs.getDeviceId());

        }
        call.enqueue(new Callback<SignInUser>() {
            @Override
            public void onResponse(Call<SignInUser> call, Response<SignInUser> response) {
                if (response.isSuccessful()) {
                    SignInUser signInUser = response.body();
                    BusProvider.getInstance().post(signInUser);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<SignInUser> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public static void forgot(String email, String phone) {
        Call<Forgot> call = ApiController.getApiService().forgot(email, phone);
        call.enqueue(new Callback<Forgot>() {
            @Override
            public void onResponse(Call<Forgot> call, Response<Forgot> response) {
                if (response.isSuccessful()) {
                    Forgot signInUser = response.body();
                    BusProvider.getInstance().post(signInUser);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<Forgot> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }


    public static void addGuest(String first_name, String last_name, String phone,
                                String email, String date_from, String date_to,
                                String time_from, String time_to, String guest_type, ArrayList<String> days) {
        String sun = MyApplication.getContext().getResources().getString(R.string.sun);
        String mon = MyApplication.getContext().getResources().getString(R.string.mon);
        String tue = MyApplication.getContext().getResources().getString(R.string.tue);
        String wed = MyApplication.getContext().getResources().getString(R.string.wed);
        String thu = MyApplication.getContext().getResources().getString(R.string.thu);
        String fri = MyApplication.getContext().getResources().getString(R.string.fri);
        String sat = MyApplication.getContext().getResources().getString(R.string.sat);
        JsonObject data = new JsonObject();
        data.addProperty("type", guest_type);
        data.addProperty("date_start", date_from);
        data.addProperty("date_end", date_to);
        data.addProperty("time_start", time_from);
        data.addProperty("time_end", time_to);
        JsonObject restrictions = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        for (String day : days) {
            Integer val = 0;
            if (day.equals(sun)) {
                val = 0;
            }
            if (day.equals(mon)) {
                val = 1;
            }
            if (day.equals(tue)) {
                val = 2;
            }
            if (day.equals(wed)) {
                val = 3;
            }
            if (day.equals(thu)) {
                val = 4;
            }
            if (day.equals(fri)) {
                val = 5;
            }
            if (day.equals(sat)) {
                val = 6;
            }
            jsonArray.add(val);
        }
        restrictions.addProperty("first_name", first_name);
        data.add("days", jsonArray);
        restrictions.addProperty("last_name", last_name);
        restrictions.addProperty("email", email);
        restrictions.addProperty("mobile_phone", phone);
        restrictions.add("restrictions", data);

        Call<AddGuest> call = ApiController.getApiService().addGuest(MyPrefs.getUserId(MyApplication.getContext())/*, first_name, last_name, email, phone, guest_type*/, restrictions);
        call.enqueue(new Callback<AddGuest>() {

            @Override
            public void onResponse(Call<AddGuest> call, Response<AddGuest> response) {
                if (response.isSuccessful()) {
                    AddGuest object = response.body();
                    BusProvider.getInstance().post(object);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<AddGuest> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }


    public static void openDoor(String hash) {
        Log.d("Log", "123Call openDoor");
        Call<Forgot> call = ApiController.getApiService().openDoor(hash);
        call.enqueue(new Callback<Forgot>() {

            @Override
            public void onResponse(Call<Forgot> call, Response<Forgot> response) {
                if (response.isSuccessful()) {
                    Forgot object = response.body();
                    BusProvider.getInstance().post(object);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<Forgot> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public static void setToken() {
        JsonObject jsonTokens = new JsonObject();
        JsonObject jsonParams = new JsonObject();

        JsonArray jsonArray = new JsonArray();
        jsonParams.addProperty("device_id", MyPrefs.getDeviceId());
        jsonParams.addProperty("token_type", TextValues.TOKEN_TYPE_FIREBASE);
        jsonParams.addProperty("device_token", MyPrefs.getPushToken(MyApplication.getContext()));
        jsonArray.add(jsonParams);
        jsonTokens.add("tokens",jsonArray);
        Call<AddGuest> call = ApiController.getApiService().setToken(jsonTokens);
        call.enqueue(new Callback<AddGuest>() {

            @Override
            public void onResponse(Call<AddGuest> call, Response<AddGuest> response) {
                if (response.isSuccessful()) {
                    AddGuest object = response.body();
                    BusProvider.getInstance().post(object);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<AddGuest> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public static void removeToken() {
//        Call<AddGuest> call = ApiController.getApiService().removeToken(MyPrefs.getDeviceId(), MyPrefs.getPushToken(MyApplication.getContext()));
//        call.enqueue(new Callback<AddGuest>() {
//
//            @Override
//            public void onResponse(Call<AddGuest> call, Response<AddGuest> response) {
//                if (response.isSuccessful()) {
//                    AddGuest object = response.body();
//                    BusProvider.getInstance().post(object);
//                } else {
//                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<AddGuest> call, Throwable t) {
//                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
//            }
//        });
    }

    public static void getGuests() {
        Call<GuestList> call = ApiController.getApiService().getGuests(MyPrefs.getUserId(MyApplication.getContext()));
        call.enqueue(new Callback<GuestList>() {

            @Override
            public void onResponse(Call<GuestList> call, Response<GuestList> response) {
                if (response.isSuccessful()) {
                    GuestList object = response.body();
                    BusProvider.getInstance().post(object);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<GuestList> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public static void removeGuest(String guestID) {
        Call<RemoveGuest> call = ApiController.getApiService().removeGuest(MyPrefs.getUserId(MyApplication.getContext()), guestID);
        call.enqueue(new Callback<RemoveGuest>() {

            @Override
            public void onResponse(Call<RemoveGuest> call, Response<RemoveGuest> response) {
                if (response.isSuccessful()) {
                    RemoveGuest object = response.body();
                    BusProvider.getInstance().post(object);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<RemoveGuest> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public static void getGuestByID(String guestID) {
        Call<Guest> call = ApiController.getApiService().getGuestByID(MyPrefs.getUserId(MyApplication.getContext()), guestID);
        call.enqueue(new Callback<Guest>() {

            @Override
            public void onResponse(Call<Guest> call, Response<Guest> response) {
                if (response.isSuccessful()) {
                    Guest object = response.body();
                    BusProvider.getInstance().post(object);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<Guest> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public static void resendInviteGuest(String guestID) {
        Call<ResendInvite> call = ApiController.getApiService().resendInviteGuest(MyPrefs.getUserId(MyApplication.getContext()), guestID);
        call.enqueue(new Callback<ResendInvite>() {

            @Override
            public void onResponse(Call<ResendInvite> call, Response<ResendInvite> response) {
                if (response.isSuccessful()) {
                    ResendInvite object = response.body();
                    BusProvider.getInstance().post(object);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<ResendInvite> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public static void editGuest(String guestId, String first_name, String last_name, String phone,
                                 String email, String date_from, String date_to,
                                 String time_from, String time_to, String guest_type, ArrayList<String> days) {
        String sun = MyApplication.getContext().getResources().getString(R.string.sun);
        String mon = MyApplication.getContext().getResources().getString(R.string.mon);
        String tue = MyApplication.getContext().getResources().getString(R.string.tue);
        String wed = MyApplication.getContext().getResources().getString(R.string.wed);
        String thu = MyApplication.getContext().getResources().getString(R.string.thu);
        String fri = MyApplication.getContext().getResources().getString(R.string.fri);
        String sat = MyApplication.getContext().getResources().getString(R.string.sat);
        JsonObject data = new JsonObject();
        data.addProperty("type", guest_type);
        data.addProperty("date_start", date_from);
        data.addProperty("date_end", date_to);
        data.addProperty("time_start", time_from);
        data.addProperty("time_end", time_to);
        JsonObject restrictions = new JsonObject();
        JsonArray jsonArray = new JsonArray();
        for (String day : days) {
            Integer val = 0;
            if (day.equals(sun)) {
                val = 0;
            }
            if (day.equals(mon)) {
                val = 1;
            }
            if (day.equals(tue)) {
                val = 2;
            }
            if (day.equals(wed)) {
                val = 3;
            }
            if (day.equals(thu)) {
                val = 4;
            }
            if (day.equals(fri)) {
                val = 5;
            }
            if (day.equals(sat)) {
                val = 6;
            }
            jsonArray.add(val);
        }
        restrictions.addProperty("first_name", first_name);
        data.add("days", jsonArray);
        restrictions.addProperty("last_name", last_name);
        restrictions.addProperty("email", email);
        restrictions.addProperty("mobile_phone", phone);
        restrictions.add("restrictions", data);

        Call<AddGuest> call = ApiController.getApiService().editGuest(MyPrefs.getUserId(MyApplication.getContext()), guestId, restrictions);
        call.enqueue(new Callback<AddGuest>() {

            @Override
            public void onResponse(Call<AddGuest> call, Response<AddGuest> response) {
                if (response.isSuccessful()) {
                    AddGuest object = response.body();
                    BusProvider.getInstance().post(object);
                } else {
                    sendErrorEvent(MyApplication.getContext().getString(R.string.error_message));
                }
            }

            @Override
            public void onFailure(Call<AddGuest> call, Throwable t) {
                sendErrorEvent(MyApplication.getContext().getString(R.string.error_network_message));
            }
        });
    }

    public static void sendErrorEvent(String message) {
        ErrorEvent errorEvent = new ErrorEvent();
        errorEvent.setErrorMessage(message);
        BusProvider.getInstance().post(errorEvent);
    }
}
