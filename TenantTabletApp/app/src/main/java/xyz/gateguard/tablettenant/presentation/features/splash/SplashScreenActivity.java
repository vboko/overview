package xyz.gateguard.tablettenant.presentation.features.splash;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.WindowManager;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;
import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.buzz.activity.BuzzActivity;
import xyz.gateguard.tablettenant.presentation.features.login.activity.SignInActivity;
import xyz.gateguard.tablettenant.presentation.features.tools.activity.ToolsServicesActivity;
import xyz.gateguard.tablettenant.util.Localization;
import xyz.gateguard.tablettenant.util.MyPrefs;


public class SplashScreenActivity extends BaseActivity {
    private static final String TAG = "SplashScreenActivity";
    private final int TIME_SPLASH = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_splash_screen);

//        if (savedInstanceState == null) {
//            getFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment())
//                    .commit();
//        }

//        Intent intent = new Intent(Intent.ACTION_MAIN);
//        intent.addCategory(Intent.CATEGORY_HOME);
//        ResolveInfo resolveInfo = getPackageManager().resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
//        String currentHomePackage = resolveInfo.activityInfo.packageName;
//
//        Log.d(TAG, "current home package " + currentHomePackage);


//        if (savedInstanceState == null) {
//            getFragmentManager().beginTransaction()
//                    .add(R.id.container, new PlaceholderFragment())
//                    .commit();
//        }

        if (MyPrefs.getLang(this).length() > 0) {
            Localization.setLocale(MyPrefs.getLang(this), SplashScreenActivity.this);
        }

        if (getIntent().getStringExtra("hash") != null) {
            startBuzzActivity(getIntent().getStringExtra("hash"));
        } else
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
//                startToolsScreen();

                    if (MyPrefs.getToken(SplashScreenActivity.this).isEmpty()) {
                        startSignInActivity();
                    } else {
                        startToolsScreen();
                    }
                }
            }, TIME_SPLASH);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
    }

    private void startSignInActivity() {
//        AddGuestActivity.startActivity(SplashScreenActivity.this);
        SignInActivity.startActivity(SplashScreenActivity.this);
//        AddGuestActivity.startActivity(SplashScreenActivity.this);
//        BuzzActivity.startActivity(SplashScreenActivity.this);
        finish();
    }

    private void startBuzzActivity(String hash) {
        Intent intent = new Intent(this, BuzzActivity.class);
        intent.putExtra("hash", hash);
        startActivity(intent);
        finish();
    }

    private void startToolsScreen() {
        ToolsServicesActivity.startActivity(SplashScreenActivity.this);
        finish();
    }
}
