package xyz.gateguard.tablettenant.presentation.features.guests.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.data.bus.BusProvider;
import xyz.gateguard.tablettenant.data.model.ClickItem;
import xyz.gateguard.tablettenant.data.model.GuestList;

/**
 * Created by Dmitriy Boichuk on 13.05.2017.
 */

public class GuestListAdapter extends RecyclerView.Adapter<GuestListAdapter.ViewHolder> {
    Context context;
    List<GuestList.Datum> mDataset;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;


        public ViewHolder(View v) {
            super(v);

            tvTitle = (TextView) v.findViewById(R.id.tvTitle);
        }
    }

    public GuestListAdapter(Context context, List<GuestList.Datum> dataset) {
        mDataset = dataset;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_guest_title, parent, false);
        ViewHolder vh = new ViewHolder(v);

        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final GuestList.Datum item = mDataset.get(position);
        GuestList.GuestData guestData = item.getGuestData();
        holder.tvTitle.setText(guestData.getFirstName() + " " + guestData.getLastName());

        holder.tvTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                BusProvider.getInstance().post(new ClickItem(item.getGuestUniqueId()));
//                Intent intent = new Intent(context, AriclesActivity.class);
//                intent.putExtra("id", item.getIID());
//                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }
}
