package xyz.gateguard.tablettenant.presentation.features.language.activity;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.widget.Toast;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseActivity;
import xyz.gateguard.tablettenant.presentation.features.language.fragment.LanguageFragment;


public class LanguageActivity extends BaseActivity {

    public static void startActivity(Context activity) {
        Intent intent = new Intent(activity, LanguageActivity.class);
        activity.startActivity(intent);
     }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.base_layout);
//        Thread.setDefaultUncaughtExceptionHandler(new DefaultExceptionHandler(this));
        setCurrentLocale();
        startLanguageFragment();
    }

    private void startLanguageFragment() {
        LanguageFragment languageFragment = (LanguageFragment) getSupportFragmentManager().findFragmentByTag(LanguageFragment.TAG);
        if (languageFragment == null) {
            languageFragment = LanguageFragment.newInstance();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.empty_frame_layout, languageFragment, LanguageFragment.TAG)
                    .commit();
        }
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        // refresh your views here
        Toast.makeText(this, "onConfigurationChanged LANG LanguageActivity", Toast.LENGTH_SHORT).show();
        super.onConfigurationChanged(newConfig);
    }
}
