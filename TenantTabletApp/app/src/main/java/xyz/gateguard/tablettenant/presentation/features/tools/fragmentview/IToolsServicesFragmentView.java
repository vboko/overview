package xyz.gateguard.tablettenant.presentation.features.tools.fragmentview;

public interface IToolsServicesFragmentView {

    void setListener(Listener listener);

    interface Listener {
        void back();

        void buzz();

        void onAddGuestClick();

        void onEditGuestClick();

        void onSchedulePickupClick();

        void onOrderLaundryClick();

        void onOrderCleaningClick();

        void onSendLockedCodeClick();

        void onUpdateMyAccountClick();

        void onUpdateMyPaymentInfoClick();
    }
}
