package xyz.gateguard.tablettenant.common;

public interface Notifiable {
	void notifyChanged();
}
