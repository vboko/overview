package xyz.gateguard.tablettenant.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalendarUtils {

    public static final String VIEW_DATE_FORMAT = "MMM dd, yyyy";

    public static String getDate(Calendar calendar) {
        Date date = new Date(calendar.getTimeInMillis());
        DateFormat formatter = new SimpleDateFormat(VIEW_DATE_FORMAT);
        return formatter.format(date);
    }



}

