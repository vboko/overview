package xyz.gateguard.tablettenant.presentation.features.schedulepickup.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import xyz.gateguard.tablettenant.R;
import xyz.gateguard.tablettenant.presentation.base.BaseSupportFragment;
import xyz.gateguard.tablettenant.presentation.features.links.activity.LinksActivity;
import xyz.gateguard.tablettenant.presentation.features.schedulepickup.fragmentview.ISchedulePickupFragmentView;
import xyz.gateguard.tablettenant.presentation.features.schedulepickup.fragmentview.SchedulePickupFragmentView;

public class SchedulePickupFragment extends BaseSupportFragment {

    public static final String TAG = SchedulePickupFragment.class.getSimpleName();

    private SchedulePickupFragmentView schedulePickupFragmentView;


    public static SchedulePickupFragment newInstance() {
        return new SchedulePickupFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.schedule_pickup_fragment_view, container, false);
        schedulePickupFragmentView = (SchedulePickupFragmentView) rootView;
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        schedulePickupFragmentView.setListener(new ISchedulePickupFragmentView.Listener() {
            @Override
            public void back() {
                getActivity().onBackPressed();
            }

            @Override
            public void onUpsClick() {
                LinksActivity.startActivity(getActivity(), "https://www.google.com");
            }

            @Override
            public void onFedexClick() {
                LinksActivity.startActivity(getActivity(), "https://www.google.com");
            }

            @Override
            public void onDhlClick() {
                LinksActivity.startActivity(getActivity(), "https://www.google.com");
            }

            @Override
            public void onLaundryLimoClick() {

            }

            @Override
            public void onDryCleanExpressClick() {

            }

            @Override
            public void onCleanlyClick() {

            }

            @Override
            public void onAmazonClick() {

            }

            @Override
            public void onMacysClick() {

            }

            @Override
            public void onCostcoClick() {

            }

            @Override
            public void onLennysClick() {

            }

            @Override
            public void onStreetShoeRepairClick() {

            }

            @Override
            public void onQuestMedicalClick() {

            }
        });
    }
}
