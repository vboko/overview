package xyz.gateguard.tablettenant.presentation.base;

import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.util.Log;

import xyz.gateguard.tablettenant.data.bus.BusProvider;
import xyz.gateguard.tablettenant.presentation.widgets.ProgressDialog;

public class BaseFragment extends Fragment {
    private Dialog progressDialog;

    public void showProgressDialog(Context context) {
        if (progressDialog == null)
            progressDialog = ProgressDialog.getProgressDialog(context);
        progressDialog.show();
    }

    public void dismissProgressDialog() {
        Log.d("Log","dismissProgressDialog");
        if (progressDialog != null)
            progressDialog.dismiss();
    }


    @Override
    public void onStart() {
        super.onStart();
        BusProvider.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        BusProvider.getInstance().unregister(this);
    }

}
